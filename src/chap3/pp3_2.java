package chap3;

import java.util.Scanner;
import java.util.StringTokenizer;

public class pp3_2 {
    public static void main(String[] args) {
        ArrayStack stack = new ArrayStack();
        Scanner scan = new Scanner(System.in);
        String another = "y";
        while (another.equalsIgnoreCase("y")) {
            System.out.print("输入一个句子:");
            String sentence = scan.nextLine();
            StringTokenizer str = new StringTokenizer(sentence);
            System.out.print("倒叙输出:");
            while(str.hasMoreElements())
        {
            String token = str.nextToken();
            int a = token.length();
            for(int i = 0; i < a; i++)
                stack.push(token.charAt(i));
            for(int i = 0;i < a;i++) {
                System.out.print(stack.pop() + "");
            }
            System.out.print(" ");
        }
        System.out.println();
        System.out.print("继续输入？(y/n)");
        another = scan.nextLine();
        System.out.println();
    }
}
}