package chap3;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class ArrayStackTest {
    ArrayStack stack = new ArrayStack();
    public ArrayStackTest(){}



    @Test
    public void peek() throws Exception{
        stack.push("1");
        Assert.assertEquals("1",stack.peek());
        stack.push("11");
        stack.push("12");
        Assert.assertEquals("12",stack.peek());
    }

    @Test
    public void isEmpty() {
        stack.push("1");
        Assert.assertEquals(false,stack.isEmpty());
    }

    @Test
    public void size() {
        stack.push("1");
        stack.push("2");
        stack.push("3");
        Assert.assertEquals(3,stack.size());
    }

//    //@Test
//    public String toString() {
//
//        stack.push("1");
//        stack.push("2");
//        Assert.assertEquals("12",stack.toString());
//        return stack[index];
//    }
}