package chap3;

import chap3.*;

import java.lang.reflect.Array;
import java.util.Arrays;

public class ArrayStack<T> implements StackADT<T>
{
    private final int DEFAULT_CAPACITY=100;

    private  int top,count;
    private  T[] stack;

    public ArrayStack()
    {
        top = 0;
        stack = (T[]) (new Object[DEFAULT_CAPACITY]);
    }

    public ArrayStack(int initialCapacity)
    {
        top = 0;
        stack = (T[])(new Object[initialCapacity]);
    }


    public  void push(T element)
    {
        if(size() == stack.length)
            expandCapacity();
        stack[top] = element;
        top++;
    }

    private void expandCapacity() {
        stack = Arrays.copyOf(stack,stack.length*2);
    }


    public T pop() throws EmptyCollectionException {
        if (isEmpty())
            throw new EmptyCollectionException("Stack");
        top--;
        T result = stack[top];
        stack[top] = null;

        return result;
    }

    @Override
    public T peek() throws EmptyCollectionException{
        if(isEmpty()){
            throw new EmptyCollectionException("Stack");
        }
        return stack[top-1];
    }

    @Override
    public boolean isEmpty() {
        if(top == 0){
            return true;
        }else {
            return false;
        }
    }

    @Override
    public int size() {
        return top;
    }

    public String toString(){
        String result = " ";
        for (int index = 0;index<size();index++){
            result =result+stack[index]+"\n";
        }
        return result;
    }

}