package 实验2;
import chap10.LinkedBinaryTree;

import java.util.*;
public class infixToSuffix {
    private Stack<LinkedBinaryTree<String>> treeExpression;
    private Stack ope= new Stack();
    private int value;

    public infixToSuffix()
    {
        value = 0;
        treeExpression = new Stack<LinkedBinaryTree<String>>();
    }
    public String infixToSuffix(String s)
    {
        String result="";
        StringTokenizer stringTokenizer = new StringTokenizer(s);

        while (stringTokenizer.hasMoreTokens())
        {
            String m = stringTokenizer.nextToken();
            if (isOperator(m)) {

                if (m.equals("*") || m.equals("/"))
                    ope.push(m);
                else if (ope.empty())
                    ope.push(m);
                else {
                    while (!ope.isEmpty()) {
                        String s1 = String.valueOf(ope.pop());
                        LinkedBinaryTree operand3 = treeExpression.pop();
                        LinkedBinaryTree operand4 = treeExpression.pop();
                        LinkedBinaryTree<String> linkedBinaryTree1 = new LinkedBinaryTree<String>(s1, operand4, operand3);
                        treeExpression.push(linkedBinaryTree1);
                        if (ope.isEmpty())
                            break;
                    }
                    ope.push(m);
                }
            }


            else
            {
                LinkedBinaryTree<String> linkedBinaryTree3 = new LinkedBinaryTree<String>(m);
                treeExpression.push(linkedBinaryTree3);
            }
        }
        value = ope.size();
        for (int y = 0; y < value; y++)
        {
            String s2  = String.valueOf(ope.pop());
            LinkedBinaryTree operand5 = treeExpression.pop();
            LinkedBinaryTree operand6 = treeExpression.pop();
            LinkedBinaryTree<String> linkedBinaryTree1 = new LinkedBinaryTree<String>(s2,operand6,operand5);
            treeExpression.push(linkedBinaryTree1);
        }

        LinkedBinaryTree linkedBinaryTree =  treeExpression.pop();

        System.out.println("这棵树为："+linkedBinaryTree.toString());
        Iterator iterator = linkedBinaryTree.iteratorPostOrder();
        while (iterator.hasNext())
            result += iterator.next()+ " ";

        return result;
    }
    private boolean isOperator(String token) {
        return (token.equals("+") || token.equals("-") || token.equals("*") || token
                .equals("/"));
    }
}
