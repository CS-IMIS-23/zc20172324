package 实验2;

import static 实验2.TraversalTransition.*;

public class TraversalTransitionTest {
        public static void main(String[] args){
            //中序HDIBEMJNAFCKGL
            String Inorder[]={"H","D","I","B","E","M","J","N","A","F","C","K","G","L"};
            //后序ABDHIEJMNCFGKL
            String Postorder[]={"A","B","D","H","I","E","J","M","N","C","F","G","K","L"};
            BinaryTreeNode NewTree = BuildBinaryTree(Inorder,Postorder);
            System.out.print("已知中序遍历和后序遍历分别为:");
            InorderTree(NewTree);
            System.out.print("和");
            PostorderTree(NewTree);
            System.out.println();
            System.out.print("得到它的唯一先序遍历结果: ");
            PreorderTree(NewTree);
            System.out.println();

        }
    }


