package 实验2;

import chap10.PostfixEvaluator;
import java.util.Scanner;
public class InfixToSuffixTest
{
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        infixToSuffix infixToSuffix = new infixToSuffix();
        System.out.println("请输入一个中缀表达式,请用空格隔开：");
        String exp = scan.nextLine();
        //infixToSuffix.infixToSuffix(exp);
        String x = infixToSuffix.infixToSuffix(exp);
        System.out.println("后缀表达式为： "+x);
        PostfixEvaluator evaluator = new PostfixEvaluator();
        int result =  evaluator.evaluate(x);
        System.out.println("计算结果为："+result);

    }

}
