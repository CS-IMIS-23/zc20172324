package 实验2;
public class TraversalTransition {
    //重建一个新的二叉树
    //给出中序HDIBEMJNAFCKGL和后序ABDHIEJMNCFGKL,构造出附图中的树
    public static BinaryTreeNode BuildBinaryTree(String[] inorder, String[] preorder) {
        if (inorder == null || preorder == null) {
            return null;
        }
        BinaryTreeNode a = BuildBinaryTreeKey(inorder, preorder, 0, inorder.length - 1, 0, preorder.length - 1);
        return a;
    }
    //建立递归函数
    public static BinaryTreeNode BuildBinaryTreeKey(String[] inorder, String[] preorder, int inStart, int inEnd, int preStart, int preEnd) {
        BinaryTreeNode BTN = new BinaryTreeNode(preorder[preStart]);
        BTN.left = null;
        BTN.right = null;
        if (inStart == inEnd && preStart == preEnd) {
            return BTN;
        }
        int root = 0;
        for (root = inStart; root < inEnd; root++) {
            if (preorder[preStart] == inorder[root]) {
                break;//用于找到中序遍历中根节点的位置
            }
        }


        int Length = root - inStart;
        int rightLength = inEnd - root;
        if (Length > 0) {
            BTN.left = BuildBinaryTreeKey(inorder, preorder, inStart, root - 1, preStart + 1, preStart + Length);
        }
        if (rightLength > 0) {
            BTN.right = BuildBinaryTreeKey(inorder, preorder, root + 1, inEnd, preStart + 1 + Length, preEnd);
        }
        return BTN;
    }

    //后序遍历
    public static void PostorderTree(BinaryTreeNode node) {
        if (node == null) {
            return;
        }
        System.out.print(node.node + " ");
        while (node.left != null) {
            PostorderTree(node.left);
            break;
        }
        while (node.right != null) {
            PostorderTree(node.right);
            break;
        }
    }

    //中序遍历
    public static void InorderTree(BinaryTreeNode node) {
        if (node == null) {
            return;
        }
        while (node.left != null) {
            InorderTree(node.left);
            break;
        }
        System.out.print(node.node + " ");
        while (node.right != null) {
            InorderTree(node.right);
            break;
        }
    }

    //先序遍历
    public static void PreorderTree(BinaryTreeNode node) {
        if (node == null) {
            return;
        }
        while (node.left != null) {
            PreorderTree(node.left);
            break;
        }
        while (node.right != null) {
            PreorderTree(node.right);
            break;
        }
        System.out.print(node.node + " ");
    }
}
