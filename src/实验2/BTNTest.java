package 实验2;

import chap11.NewLinkedBinarySearchTree;

public class BTNTest{
    public static void main(String[] args) {
    NewLinkedBinarySearchTree HalloweenTree = new NewLinkedBinarySearchTree();
    HalloweenTree.addElement(2);
    HalloweenTree.addElement(7);
    HalloweenTree.addElement(0);
    HalloweenTree.addElement(6);

    System.out.println(HalloweenTree.toString());
    System.out.println("添加元素1之后的二叉查找树为：");
    HalloweenTree.addElement(1);
    System.out.println(HalloweenTree.toString());
    System.out.println("树中最大的元素："+HalloweenTree.findMax());
    System.out.println("树中最小的元素："+HalloweenTree.findMin());
    HalloweenTree.removeMax();
    System.out.println("删除当前最大元素后树中最大的元素："+HalloweenTree.findMax());

}
}