package 实验2;


import chap10.LinkedBinaryTree;
import chap6.ElementNotFoundException;
import junit.framework.TestCase;
import org.junit.Test;
public class LinkedBinaryTreeTest extends TestCase {
    LinkedBinaryTree L1 = new LinkedBinaryTree(3);
    LinkedBinaryTree L2 = new LinkedBinaryTree(6);
    LinkedBinaryTree R1 = new LinkedBinaryTree(4);
    LinkedBinaryTree R2 = new LinkedBinaryTree(2);
    LinkedBinaryTree T1 = new LinkedBinaryTree(1,L1,R1);
    LinkedBinaryTree T2 = new LinkedBinaryTree(8,L2,R2);
    LinkedBinaryTree T = new LinkedBinaryTree(7,T1,T2);

    public void testGetRight() throws Exception {
        assertEquals(8, T.getRight().getRootElement());
    }

    public void testContains() throws Exception, ElementNotFoundException {
        assertEquals(true, T.contains(1));

    }
    @Test
    public void testPreorder() {
        assertEquals("[7, 1, 3, 4, 8, 6, 2]",T.preorder().toString());

    }
    public void testPostorder() throws Exception {
        assertEquals("[3, 1, 4, 6, 8, 2, 7]", T.postorder().toString());

    }

    public void testtoString() throws Exception {
        System.out.println(T.toString());

    }
}