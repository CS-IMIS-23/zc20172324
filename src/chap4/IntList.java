package chap4;



public class IntList
{
    private IntNode list;
    public IntList()
    {
        list = null;
    }
    int count = 0;


    public void add(Int mag)
    {
        IntNode node = new IntNode(mag);
        count++;
        IntNode current;

        if (list == null)
            list = node;
        else
        {
            current = list;
            while (current.next != null)
                current = current.next;
            current.next = node;
        }
    }

    public void delete(Int mag){
        IntNode node = new IntNode(mag);
        IntNode current, temp;

        if(list.INT.number == mag.number) {
            current = list.next;
            list = current;
        }
        else{
            current = list;
            while(current.next.INT.number != mag.number)
                current = current.next;
            temp = current.next.next;
            current.next = temp;
        }

        count --;
    }

    public void insert(Int num1, Int num2){
        IntNode node1 = new IntNode(num1);
        IntNode node2 = new IntNode(num2);
        IntNode current;

        if(list.INT.number == num2.number) {
            current = list;
            node1.next = current;
            current.next = node1;
        }
        else{
            current = list;
            while(current.next.INT.number != num2.number)
                current = current.next;
            node1.next = current.next;
            current.next = node1;

        }

        count++;
    }

    public void Sort(){
        IntNode current;
        current = list;
        int[] A = new int[count];
        for(int i = 0; i < count; i++) {
            A[i] = current.INT.number;
            current = current.next;
        }

        Sorting sort = new Sorting();
        int[] B = Sorting.selectionSort(A);

        list = null;
        int count2 = count;
        for(int i =0;i< count2; i++){
            Int num = new Int(B[i]);
            add(num);
        }
    }

    public String toString()
    {
        String result = "";

        IntNode current = list;

        while (current != null)
        {
            result += current.INT + " ";
            current = current.next;
        }

        return result;
    }

    private class IntNode
    {
        public Int INT;
        public IntNode next;
        public IntNode(Int mag)
        {
            INT = mag;
            next = null;
        }
    }
}
