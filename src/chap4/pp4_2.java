package chap4;


import java.util.Scanner;

public class pp4_2 {
    public static void main(String[] args) {
        String expression,again;
        int result;

        Scanner in =new Scanner(System.in);

        do {
            Postfix evaluator =new Postfix() ;

            System.out.println("Enter a valid post-fix expression one token"+"at a time with a space between each token(e.g.5 4 + 3 2 1 - + *)");
            expression = in.nextLine() ;

            result = evaluator .evaluate(expression );
            System.out.println();
            System.out.println("That expression equals "+result );

            System.out.println("Evaluate another expression [Y/N]?");
            again = in.nextLine();
            System.out.println();
        }
        while (again.equalsIgnoreCase("y") );
    }
}
