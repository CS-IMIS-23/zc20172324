package chap4;

public class LinkedStackTest {
    public static void main(String[] args) {


    LinkedStack a = new LinkedStack();
    a.push(4);
    a.push(2);
    a.push(3);
    a.push(9);


        System.out.println("栈中所有元素："+"\n"+a.toString());
        System.out.println("开始的容量："+a.size());
        System.out.println("是否是空的："+a.isEmpty());

        System.out.println("弹出栈顶元素："+a.pop());
        System.out.println("现在的容量："+a.size());
        System.out.println("是否是空的："+a.isEmpty());
        System.out.println("现在的栈顶元素："+a.peek());
        System.out.println("现在栈中所有元素："+"\n"+a.toString());

        System.out.println("弹出栈顶元素："+a.pop());
        System.out.println("弹出栈顶元素："+a.pop());
        System.out.println("弹出栈顶元素："+a.pop());
        System.out.println("是否是空的："+a.isEmpty());
}
 }