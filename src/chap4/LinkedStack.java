package chap4;

import chap3.EmptyCollectionException;
import chap3.StackADT;
public class LinkedStack<T> implements StackADT<T> {
    private final int DEFAULT_CAPACITY=100;

    private LinearNode top;
    private int count;
    private  T[] stack;
    @Override
    public T pop() throws EmptyCollectionException {
        if (this.isEmpty()) {
            throw new EmptyCollectionException("Stack");
        } else {
            T result = (T) this.top.getElement();
            this.top = this.top.getNext();
            this.count--;
            return result;
        }
    }

    @Override
    public void push(T element) {
        LinearNode<T> temp = new LinearNode(element);
        temp.setNext(this.top);
        this.top = temp;
        this.count++;
    }
    @Override
    public T peek() throws EmptyCollectionException {
        if (this.isEmpty()) {
            throw new EmptyCollectionException("Stack");
        } else {
            return (T) this.top.getElement();
        }
    }
    @Override
    public boolean isEmpty() {
        return this.count == 0;
    }
    @Override
    public int size() {
        return this.count;
    }
    @Override
    public String toString() {
        String result = "";

        for(LinearNode current = this.top; current != null; current = current.getNext()) {
            result = result + current.getElement().toString() + "\n";
        }
        return result;
    }

}
