package chap4;

import java.util.Scanner;


public class Postfix {
    private final static char ADD = '+';
    private final static char SUBTRACT='-';
    private final static char MULTIPLY='*';
    private final static char DIVIDE='/';

    private LinkedStack<Integer> LinkedStack ;

    public Postfix(){
        LinkedStack=new LinkedStack<Integer>() ;
    }
    public int evaluate(String expr){
        int op1,op2,result = 0;
        String token;
        Scanner parser = new Scanner(expr);

        while (parser.hasNext() ) {
            token = parser.next();

            if (isOperator(token)) {
                op2 = (LinkedStack.pop()).intValue();

                op1 = (LinkedStack.pop()).intValue();
                result = evaluateSingleOperator(token.charAt(0), op1, op2);
                LinkedStack.push(new Integer(result));

            } else
                LinkedStack.push(new Integer(Integer.parseInt(token)));
        }

        return result;
    }
    private boolean isOperator(String token){
        return (token .equals("+") || token .equals("-") || token .equals("*") || token .equals("/") );
    }

    private int evaluateSingleOperator (char operation ,int op1,int op2){
        int result=0;

        switch (operation){
            case ADD:
                result =op1+op2;
                break;
            case SUBTRACT :
                result =op1-op2;
                break;
            case MULTIPLY :
                result =op1*op2;
                break;
            case DIVIDE :
                result =op1/op2;
        }

        return result ;
    }
}
