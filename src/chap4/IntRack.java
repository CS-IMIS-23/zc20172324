package chap4;

import java.util.Scanner;

public class IntRack
{
    //----------------------------------------------------------------
    //  Creates a MagazineList object, adds several magazines to the
    //  list, then prints it.
    //----------------------------------------------------------------
    public static void main(String[] args)
    {

        Scanner scan = new Scanner(System.in);
        System.out.println("请输入五个整数（用空格分开）:");
        int num1,num2,num3,num4,num5;
        num1 = scan.nextInt();
        num2 = scan.nextInt();
        num3 = scan.nextInt();
        num4 = scan.nextInt();
        num5 = scan.nextInt();

        IntList rack = new IntList();
        System.out.print("建立链表：");
        rack.add(new Int(num1));
        rack.add(new Int(num2));
        rack.add(new Int(num3));
        rack.add(new Int(num4));
        rack.add(new Int(num5));
        System.out.println(rack);

        IntList rack2 = rack;
        System.out.print("删除第二位：");
        rack2.delete(new Int(num2));
        System.out.println(rack2);

        IntList rack3 = rack2;
        System.out.print("将第三位数字插入第四位数字之前：");
        rack3.insert(new Int(num3),new Int(num4));
        System.out.println(rack3);

        IntList rack4 = rack3;
        System.out.print("进行排序：");
        rack4.Sort();
        System.out.println(rack4);
    }
}
