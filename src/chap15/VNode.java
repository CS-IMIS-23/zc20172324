package chap15;

public class VNode<T> {
    private T element;
    private VNode<T> next;
    public VNode(T element)
    {
        this.element = element;
        next = null;
    }
    public void setNext(VNode<T> vertex){
        next = vertex;
    }
    public VNode<T> getNext(){
        return next;
    }
    public T getElement(){
        return element;
    }
}
