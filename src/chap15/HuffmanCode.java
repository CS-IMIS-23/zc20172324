package chap15;

import java.io.*;
import java.util.*;

public class HuffmanCode
{
    //哈夫曼编码和解码
    public static void main(String[] args) throws Exception
    {

        //读取文件中出现的字符，并记录出现次数
        File file = new File("C:\\Users\\96553\\IdeaProjects\\zc\\src\\chap15\\text");

        if (!file.exists()) {
            throw new Exception("文件不存在");
        }

        BufferedReader fin = new BufferedReader(new FileReader(file));
        String line;

        //map储存数据的形式是一个key和一个value对应
        Map<Character, Integer> counter = new HashMap<Character, Integer>();

        int total=0;

        //读取一个文本行
        while ((line = fin.readLine()) != null)
        {
            int len = line.length();
            for (int i = 0; i < len; i++)
            {
                char c = line.charAt(i);
                if (( (c >= 'a' && c <= 'z'&& c == ' ')))
                {
                    continue;
                }

                // 如果此映射包含指定键的映射关系，则返回 true
                if (counter.containsKey(c))
                {
                    counter.put(c, counter.get(c) + 1);
                }
                else
                {
                    counter.put(c, 1);
                }
            }
        }

        fin.close();//文件读取结束

        //通过Map.keySet遍历key和value：
        Iterator<Character> pl = counter.keySet().iterator();

        int a = 0;
        while (pl.hasNext())
        {
            char key = pl.next();
            int count = counter.get(key);
//            System.out.println(key + " --- " + count);
            a++;
        }

        //层序遍历显示构建的哈弗曼树
        char[] chars = new char[a];
        int[] times = new int[a];
        Iterator<Character> pl2 = counter.keySet().iterator();

        for (int i=0;i<=a;i++ )
        {
            if (pl2.hasNext())
            {
                chars[i] = pl2.next();
                times[i] = counter.get(chars[i]);
            }

        }
        List<Node> list = new ArrayList<Node>();

        for(int i = 0;i<a;i++)
        {
            System.out.print(chars[i]+"："+times[i]+"   ");
            list.add(new Node<String>(chars[i]+"",times[i]));
        }

        List<Node> list2 = new ArrayList<Node>();
        List<String> list3 = new ArrayList<String>();
        List<String> list4 = new ArrayList<String>();

        HuffmanTree huffmanTree = new HuffmanTree();
        Node<String> root = huffmanTree.createTree(list);

        list2 = huffmanTree.levelTraversal(root);

        for(int i = 0;i<list2.size();i++)
        {
            if(list2.get(i).getData()!=null)//节点中存放字母
            {
                list3.add(list2.get(i).getData()+"");//存放不为null的结点
                list4.add(list2.get(i).getCodeNumber());//存放编码
            }
        }

        System.out.println();

        double num2 = 0;
        for (int i = 0; i < list2.size(); i++) {
            num2 += list2.get(i).getWeight();
        }

        for (int i = 0; i < list3.size(); i++) {
            System.out.println("字符为："+list3.get(i) + "概率为：" + list2.get(i).getWeight() / num2 + "  ");
        }

        System.out.println();
        String temp2 = "", temp3 = "";

        for (int i = 0; i < list4.size(); i++)
        {
            System.out.println(list3.get(i) + "的编码为" + list4.get(i) + " ");
        }

        System.out.println();

        //创建文件，记录编码后的原文件
        File file1 = new File("C:\\Users\\96553\\IdeaProjects\\zc\\src\\chap15\\output");

        String result="",result1 = "";

        //读出并更新
        Reader reader = new FileReader(file);

        while (reader.ready())
        {
//            for (int j = 0;j<list3.size();j++)
//            {
//                if ( (char) reader.read() == list3.get(j).charAt(0))
//                    result += list4.get(j);
//            }
            result +=(char) reader.read();
        }


        List<String> list5 = new ArrayList<String>();
        System.out.println();

        for (int i = 0; i < result.length(); i++)
        {
            for (int j = 0; j < list3.size(); j++)
            {
                if (result.charAt(i) == list3.get(j).charAt(0))
                    result1 += list4.get(j);
            }
        }

        System.out.println("编码后为：" + result1);

        //写入文件
        //写入并更新
        Writer writer = new FileWriter(file1);
        writer.write(result1);
        writer.flush();

        //解码并写入文件
        for (int i = 0; i < result1.length(); i++)
        {
            list5.add(result1.charAt(i) + "");
        }
        while (list5.size() > 0) {
            temp2 = temp2 + "" + list5.get(0);
            list5.remove(0);
            for (int i = 0; i < list4.size(); i++)
            {
                if (temp2.equals(list4.get(i))) {
                    temp3 = temp3 + "" + list3.get(i);
                    temp2 = "";
                }
            }
        }

        System.out.println();
        System.out.println("解码后" + temp3);

        writer.write("\n"+temp3);
        writer.flush();

    }
}
