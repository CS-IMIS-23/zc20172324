package chap15;

public class GraphListTest {
    public static void main(String[] args) {
        GraphList<String> graphList = new GraphList<>();

        graphList.addVertex("2");
        graphList.addVertex("3");
        graphList.addVertex("1");
        graphList.addVertex("4");


        graphList.addEdge("2","3");
        graphList.addEdge("1","2");
        graphList.removeVertex("2");
        System.out.println(graphList.toString());

    }
}