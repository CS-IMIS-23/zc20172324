package chap15;

public class Node<T>

{
    //节点类
    private T data;//数据

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    protected double weight;//权重

    protected Node<T> leftChild;
    protected Node<T> rightChild;

    public String codeNumber;

    public Node(T data , double weight)

    {
        this.data = data;
        this.weight = weight;
        this.codeNumber = "";
    }

    public String getCodeNumber() {
        return codeNumber;
    }

    public void setCodeNumber(String codeNumber) {
        this.codeNumber = codeNumber;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }
    public String toString()
    {
        return "Node[data=" + data
                + ", weight=" + weight + "，codeNumber = "+codeNumber+"]";
    }


    public int compareTo(Node<T> other) {
        if(other.getWeight() > this.getWeight()){
            return 1;
        }
        if(other.getWeight() < this.getWeight()){
            return -1;
        }
        return 0;
    }
}
