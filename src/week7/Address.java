
public class Address
{
    private String streetAddress,city,state;
    private long zipCode;
    //--------------------------------------------------------------------
    //constructor:sets up this address with the specified data.
    //---------------------------------------------------------------------
    public Address(String street,String town,String st,long zip)
    {
        streetAddress = street;
        city = town;
        state = st;
        zipCode = zip;
    }
    //---------------------------------------------------------------------
    //returns a description of this Address object.
    //---------------------------------------------------------------------
    public String toString()
    {
        String result;

        result = streetAddress+"\n";
        result += city+","+state +""+zipCode;
        return result;
    }
}

