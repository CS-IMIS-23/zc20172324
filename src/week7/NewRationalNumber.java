//修改RationalNumber类，使其实现Comparable接口。为了实现比较，首先计算两个RationalNumber对象（分子和分母）
//的浮点等价值，然后以0.0001为误差精度进行比较。

import java.lang.Comparable;
public class NewRationalNumber implements Comparable<NewRationalNumber>
{
    private int numerator,denominator;
    double number1,number2;
    public NewRationalNumber(int numer,int denom)
    {
        if (denom ==0)
            denom = 1;

        if (denom <0)
        {
            numer =numer* -1;
            numer =denom* -1;
        }

        numerator = numer;
        denominator = denom;
        reduce();
    }

    public int getNumerator()
    {
        return numerator;
    }

    public int getDenominator()
    {
        return denominator;
    }

    public NewRationalNumber reciprocal()
    {
        return new NewRationalNumber(denominator,numerator);
    }

    public NewRationalNumber add(NewRationalNumber op2)
    {
        int commonDenominator = denominator * op2.getDenominator();
        int numerator1 = numerator * op2.getDenominator();
        int numerator2 = op2.getNumerator() * denominator;
        int sum = numerator1 + numerator2;

        return new NewRationalNumber(sum,commonDenominator);
    }

    public NewRationalNumber subtract(NewRationalNumber op2)
    {
        int commonDenominator = denominator * op2.getDenominator();
        int numerator1 = numerator * op2.getDenominator();
        int numerator2 = op2.getNumerator() * denominator;
        int difference = numerator1 -numerator2;

        return new NewRationalNumber(difference,commonDenominator);
    }

    public NewRationalNumber multiply(NewRationalNumber op2)
    {
        int numer = numerator *op2.getNumerator();
        int denom = denominator * op2.getDenominator();

        return new NewRationalNumber(numer,denom);
    }

    public NewRationalNumber divide(NewRationalNumber op2)
    {
        return multiply(op2.reciprocal());
    }

    public boolean isLike(NewRationalNumber op2)
    {
        return (numerator ==op2.getNumerator() &&
                denominator == op2.getDenominator() );
    }


    public String toString()
    {
        String result;
        if(numerator ==0)
            result = "0";
        else
        if (denominator == 1)
            result = numerator + "";
        else
            result= numerator + "/"+denominator;
        return result;
    }

    private void reduce()
    {
        if (numerator !=0)
        {
            int common = gcd(Math.abs(numerator),denominator);

            numerator = numerator/common;
            denominator = denominator/common;
        }
    }

    private int gcd(int num1,int num2)
    {
        while (num1 !=num2)
            if (num1>num2)
                num1=num1 - num2;
            else
                num2 = num2 - num1;

        return num1;
    }
    public int compareTo(NewRationalNumber op2) {
        double a = numerator / denominator;
        double b = op2.getNumerator() / op2.getDenominator();
        int number;
        if (Math.abs(a - b) <= 0.0001)
            number = 0;
        else if (a - b > 0.0001)
            number = 1;
        else
            number = -1;
        return number;
    }
}
