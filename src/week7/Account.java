//允许仅使用账号名和账号建立账户，并假设初始余额为0。

import java.text.NumberFormat;

public class Account {
    private final double RATE = 0.035;
    private String name;
    private long acctNumber;
    private double newbalance;
    private final double balance = 0;



    public Account(String owner, long account)
    {
        name = owner;
        acctNumber = account;

    }

    public double depoist(double amount) {
        newbalance = balance + amount;
        return newbalance;
    }

    public double withdraw(double amount, double fee) {
        newbalance = balance - amount - fee;
        return newbalance;
    }

    public double addInterest() {
        newbalance += (balance * RATE);
        return newbalance;
    }



    public String toString(){
        NumberFormat fmt = NumberFormat.getCurrencyInstance();
        return acctNumber + "\t" + name + "\t" + fmt.format(newbalance);
    }
    public double deposit(double v) {
        return v;
    }
}

