package exp5;
import java.util.Scanner;
import exp5.MyBC;

//实现中缀转后缀及其计算
public class MyC {
    public static void main(String[] args) {
        String infix;
        System.out.println("输入你想计算的中缀表达式（不需要输入空格，enter键结束输入）：");
        Scanner scan = new Scanner(System.in);
        infix = scan.nextLine();
        String postfix,buzhou;

        MyBC theTrans = new MyBC(infix);
        postfix = theTrans.doTrans();
        // buzhou = theTrans.getA();
        System.out.print("后缀表达式为：");
        String hou = postfix;
        int length = hou.length();
        char[] value = new char[length << 1];
        for (int i=0, j=0; i<length; ++i, j = i << 1) {
            value[j] = hou.charAt(i);
            value[1 + j] = ' ';
        }
        String houzhui = new String(value);
       // System.out.println(houzhui);
        String expressions,again;
        int result;
        MyDC evaluator = new MyDC();
        expressions = houzhui;
        result =  evaluator.evaluate(expressions);//
        System.out.println("计算结果为："+result);


    }
}
