package exp5;
import javax.crypto.Cipher;
import java.io.*;
import java.net.Socket;
import java.util.Scanner;
import java.security.*;
import javax.crypto.*;
import javax.crypto.spec.SecretKeySpec;

public class SocketClient4 {
    public static void main(String[] args) throws Exception{

        //1.建立客户端Socket连接，指定服务器位置和端口
    //    Socket socket = new Socket("localhost", 8080);
       Socket socket = new Socket("172.16.43.180",8800);
       //Socket socket = new Socket("172.16.43.243",8800);

        //2.得到socket读写流
        OutputStream outputStream = socket.getOutputStream();
        //       PrintWriter printWriter = new PrintWriter(outputStream);
        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(outputStream);
        //输入流
        InputStream inputStream = socket.getInputStream();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
        //3.利用流按照一定的操作，对socket进行读写操作
        System.out.println("输入中缀表达式：");
        Scanner scan =new Scanner(System.in);
        String info1 =scan.nextLine();
        //String info1 = " 用户名：Tom，密码：123456";
        MyBC theTrans = new MyBC(info1);
        String postfix = theTrans.doTrans();
        System.out.println("得到后缀表达式：" + postfix);
        String c = postfix;
        //读取对方的DH公钥
        FileInputStream f1 = new FileInputStream("Bpub.dat");
        ObjectInputStream b1 = new ObjectInputStream(f1);
        PublicKey pbk = (PublicKey) b1.readObject();
        //读取自己的DH私钥
        FileInputStream f2 = new FileInputStream("Apri.dat");
        ObjectInputStream b2 = new ObjectInputStream(f2);
        PrivateKey prk = (PrivateKey) b2.readObject();
        // 执行密钥协定
        KeyAgreement ka = KeyAgreement.getInstance("DH");
        ka.init(prk);
        ka.doPhase(pbk, true);
        //生成共享信息
        byte[] sb = ka.generateSecret();
        for (int i = 0; i < sb.length; i++) {
            System.out.print(sb[i] + ",");
        }
        SecretKeySpec k = new SecretKeySpec(sb,0,24, "DESede");
        //加密
        Cipher cp = Cipher.getInstance("DESede");
        cp.init(Cipher.ENCRYPT_MODE, k);
        //获取明文
        byte ptext[] = c.getBytes("UTF8");
        //加密
        byte ctext[] = cp.doFinal(ptext);
        String d = "";
        for (int i = 0; i < ctext.length; i++) {
            d += ctext[i] + ",";
        }
        System.out.println(d);
        String info2 = d;
        String info = new String(info2.getBytes("utf-8"));
        outputStreamWriter.write(info);
        outputStreamWriter.flush();
        socket.shutdownOutput();
        //接收服务器的响应
        String reply = null;
        while (!((reply = bufferedReader.readLine()) ==null)){
            System.out.println("计算后缀表达式：" + reply);
        }
        //4.关闭资源
        bufferedReader.close();
        inputStream.close();
        outputStreamWriter.close();
        //printWriter.close();
        outputStream.close();
        socket.close();
    }
}