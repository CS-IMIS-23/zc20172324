package exp5;



import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

import com.sun.corba.se.impl.orbutil.ObjectUtility;

import java.io.*;
import java.net.Socket;
import java.util.Scanner;

/**
 * Created by besti on 2018/6/9.
 */
public class SocketClient {
    public static void main(String[] args) throws IOException {
        //1.建立客户端Socket连接，指定服务器位置和端口
//        Socket socket = new Socket("localhost",8080);
        Socket socket = new Socket("172.16.43.243",8800);
      //  Socket socket = new Socket("172.16.43.180", 8800);

        //2.得到socket读写流
        OutputStream outputStream = socket.getOutputStream();
        //       PrintWriter printWriter = new PrintWriter(outputStream);
        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(outputStream);
        //输入流
        InputStream inputStream = socket.getInputStream();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
        //3.利用流按照一定的操作，对socket进行读写操作

        String infix;
        System.out.println("输入你想计算的中缀表达式（不需要输入空格，enter键结束输入）：");
        Scanner scan = new Scanner(System.in);
        infix = scan.nextLine();
        String postfix,buzhou;

        MyBC theTrans = new MyBC(infix);
        postfix = theTrans.doTrans();
        // buzhou = theTrans.getA();
        System.out.print("答案：");

        String info1 = postfix;
        String info = new String(info1.getBytes("GBK"), "utf-8");
        //     printWriter.write(info);
        //     printWriter.flush();
        outputStreamWriter.write(info);
        outputStreamWriter.flush();
        socket.shutdownOutput();
        //接收服务器的响应
        String reply = null;
        while (!((reply = bufferedReader.readLine()) == null)) {
            System.out.println(" " + reply);
        }
        //4.关闭资源
        bufferedReader.close();
        inputStream.close();
        outputStreamWriter.close();
        //printWriter.close();
        outputStream.close();
        socket.close();
    }
}
