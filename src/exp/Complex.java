
import static java.lang.Math.round;
import static java.lang.Math.sqrt;
class Complex

{
    private double ImagPart;
    private double RealPart;


    public Complex(double R,double I)

    {
        RealPart = R;
        ImagPart = I;
    }

//    public  double getRealPart() {
//        return RealPart;
//    }
//
//    public  double getImagPart() {
//        return ImagPart;
//    }

      public  double getRealPart()
     {
    return RealPart;

     }
    public double getImagPart()


    {
        return ImagPart;
    }
    public boolean equals(Complex a){
        boolean result = false;
        if(RealPart ==a.RealPart&&ImagPart ==a.ImagPart)
            result = true;

        return result;
    }


    public void setRealPart(int realPart) {
        RealPart = realPart;
    }

    public void setImagPart(int imagPart) {

        ImagPart = imagPart;
    }

    public Complex  myComplexAdd(Complex a){
             double s=RealPart+a.getRealPart();
             double d=ImagPart+a.getImagPart();
        return new Complex(s ,d );
    }
    public Complex myComplexSub(Complex a){
        return new Complex(RealPart-a.getRealPart(),ImagPart-a.getImagPart());
    }

    public Complex  myComplexMulti(Complex a){
        return new Complex(RealPart*a.RealPart-ImagPart*a.ImagPart,RealPart*a.ImagPart-ImagPart*a.RealPart);
    }
    public Complex  myComplexDiv(Complex a)
    {
        double b = sqrt(a.RealPart*a.ImagPart)+sqrt(a.ImagPart*a.ImagPart);
        double c = (RealPart*a.ImagPart-ImagPart*a.RealPart);
        RealPart = (RealPart*a.RealPart+ImagPart*a.ImagPart)/b;
        ImagPart = round(c/b);
        return new Complex(RealPart,ImagPart);
    }


    public String toString()
    {
        String result="";
        if(ImagPart ==0.0)
            result = RealPart+"";
        else
            if(RealPart == 0.0)
                result = ImagPart +"i";

            else
                if(ImagPart>0.0)


                    result = RealPart+""+"+"+ImagPart+"i";

                else

                    result = RealPart+""+ImagPart+"i";

        return result;




}



}

