

//public class MyDoc
// Server Classes
abstract class Data {
    abstract public void DisplayValue();
}
//0 byte
class Byte extends  Data {
    int value;
    Byte() {
        value=20172324%6;
    }
    public void DisplayValue(){
        System.out.println (value);
    }
}
//1 short
class Short extends  Data {
    int value;

    Short() {
        value = 20172324 % 6;
    }

    public void DisplayValue() {
        System.out.println(value);
    }
}
//2 Boolean
class Boolean extends  Data {
    int value;

    Boolean() {
        value = 20172324 % 6;
    }

    public void DisplayValue() {
        System.out.println(value);
    }
}
//3 long
class Long extends  Data {
    int value;

    Long() {
        value = 20172324 % 6;
    }

    public void DisplayValue() {
        System.out.println(value);
    }
}
//4 float
class Float extends  Data {
    int value;

    Float() {
        value = 20172324 % 6;
    }

    public void DisplayValue() {
        System.out.println(value);
    }
}
//5 double
class Double extends  Data {
    int value;

    Double() {
        value = 20172324 % 6;
    }

    public void DisplayValue() {
        System.out.println(value);
    }
}


// Pattern Classes
abstract class Factory {
    abstract public Data CreateDataObject();
}
//class IntFactory extends Factory {
//    public Data CreateDataObject(){
//        return new Integer();
//    }
//}
class ByteFactory extends Factory {
    public Data CreateDataObject(){
        return new Byte();
    }
}
class ShortFactory extends Factory {
    public Data CreateDataObject(){
        return new Short();
    }
}
class BooleanFactory extends Factory {
    public Data CreateDataObject(){
        return new Boolean();
    }
}
class LongFactory extends Factory {
    public Data CreateDataObject(){
        return new Long();
    }
}
class FloatFactory extends Factory {
    public Data CreateDataObject(){
        return new Float();
    }
}
class DoubleFactory extends Factory {
    public Data CreateDataObject(){
        return new Double();
    }
}
//Client classes
class Document {
    Data pd;
    Document(Factory pf){
        pd = pf.CreateDataObject();
    }
    public void DisplayData(){
        pd.DisplayValue();
    }
}
//Test class
//public class MyDoc {
//    static Document d;
//    public static void main(String[] args) {
//        d = new Document(new IntFactory());
//        d.DisplayData();
//    }
//}
public class MyDoc{
    static Document d;
    public static void main(String[] args) {
        d = new Document(new ByteFactory());
        d = new Document(new ShortFactory());
        d = new Document(new BooleanFactory());
        d = new Document(new LongFactory());
        d = new Document(new FloatFactory());
        d = new Document(new DoubleFactory());
        d.DisplayData();
    }
}
//public class MyDoc {
//    static Document d;
//    public static void main(String[] args) {
//        d = new Document(new ShortFactory());
//        d.DisplayData();
//    }
//}
//public class MyDoc {
//    static Document d;
//    public static void main(String[] args) {
//        d = new Document(new BooleanFactory());
//        d.DisplayData();
//    }
//}
//public class MyDoc {
//    static Document d;
//    public static void main(String[] args) {
//        d = new Document(new LongFactory());
//        d.DisplayData();
//    }
//}
//
//
//public class MyDoc {
//    static Document d;
//    public static void main(String[] args) {
//        d = new Document(new FloatFactory());
//        d.DisplayData();
//    }
//}
//public class MyDoc {
//    static Document d;
//    public static void main(String[] args) {
//        d = new Document(new DoubleFactory());
//        d.DisplayData();
//    }
//}
