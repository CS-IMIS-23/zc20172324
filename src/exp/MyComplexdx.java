public class MyComplexdx {
    public static void main(String[] args) {

        Complex a = new Complex(1, 2);
        Complex b = new Complex(4, 2);
        Complex c = new Complex(0, 0);
        Complex d = new Complex(-1, -2);
        Complex e = new Complex(0, -12);

        System.out.println(a.getImagPart());
        System.out.println(a.getRealPart());

        System.out.println(b.getImagPart());
        System.out.println(b.getRealPart());

        System.out.println(c.getImagPart());
        System.out.println(c.getRealPart());

        System.out.println(d.getImagPart());
        System.out.println(d.getRealPart());

        System.out.println(e.getImagPart());
        System.out.println(e.getRealPart());





        System.out.println(a.myComplexMulti(b));
        System.out.println(a.myComplexDiv(b));
        System.out.println(a.myComplexAdd(b));
        System.out.println(a.myComplexSub(b));



    }
}

