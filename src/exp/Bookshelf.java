public class Bookshelf {
    public static void main(String[] args) {
        Book book1 = new Book("diyibengshu", "diyigezuozhe", "diyigechubanshe", "2018.3.1");
        Book book2 = new Book("dierbengshu", "diyergezuozhe", "diergechubanshe","2018.3.2");
        Book book3 = new Book("disanbengshu", "disangezuozhe","disangechubanshe","2018.3.3");
        Book book4 = new Book("disibengshu", "disigezuozhe", "disigechubanshe", "2018.3.4");
        System.out.println("book1: "+ book1);
        System.out.println("book2: "+ book2);
        System.out.println("book3: "+ book3);
        System.out.println("book4: "+ book4);
    }
}
