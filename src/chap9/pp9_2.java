package chap9;


public class pp9_2 {


    public static <T extends Comparable<T>>
    void bubbleSort2(T[] data,int i )
    {
        int position, scan;
        T temp;

        for (position = i; position >= 1; position--)
        {
            for (scan = 0; scan < data.length - position; scan++)
            {
                if (data[scan].compareTo(data[scan+position]) > 0)
                    swap(data, scan, scan + position);
                scan++;
            }
            i--;
        }
    }
    static <T extends Comparable<T>>
void swap(T[] data, int index1, int index2)
{
    T temp = data[index1];
    data[index1] = data[index2];
    data[index2] = temp;
}



    public static void main(String[] args) {
       Contact[] friends = new Contact[7];

        friends[0] = new Contact("John", "Smith", "610-555-7384");
        friends[1] = new Contact("Sarah", "Barnes", "215-555-3827");
        friends[2] = new Contact("Mark", "Riley", "733-555-2969");
        friends[3] = new Contact("Laura", "Getz", "663-555-3984");
        friends[4] = new Contact("Larry", "Smith", "464-555-3489");
        friends[5] = new Contact("Frank", "Phelps", "322-555-2284");
        friends[6] = new Contact("Marsha", "Grant", "243-555-2837");

        pp9_2.bubbleSort2(friends,6);
        for (Contact friend : friends)
            System.out.println(friend);



    }

}