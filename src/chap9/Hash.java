package chap9;

public class Hash {
    public static int count = 0;//冲突次数
    int times;//初始的查找次数
    int num = 11;//添加的元素个数
    String result;
    LinearNode[] list ;

    public Hash(int num) {
        list = new LinearNode[num];
    }

    public void  ReceiveNum(int[] number) {
        for (int i = 0; i < number.length; i++) {
            LinearNode linearNode = new LinearNode(number[i]);
            int index = number[i] % num;
            LinearNode node =list[index];
            //如果当前索引值位置为空，则放入数据
            if (node == null) {
                list[index] = linearNode;
                times++;
            }
            //每次冲突的数目加一，查找的次数加一
            else
            {
                LinearNode current = list[index];
                while (current.getNext() != null)
                {
                    current = current.getNext();
                    times++;
                }
                current.setNext(linearNode);
                count++;
                times++;
            }
        }
    }

    public String toString()
    {
        result = "平均查找次数ASL："+(times+count)+"/"+num+"\n"+
                 "冲突次数为："+ count;
        System.out.println(result);
        return result;

    }

}