package chap9;


public class pp9_3 {

    static <T extends Comparable<? super T>> void swap(T[] data, int i,
                                                       int j) {
        T temp = data[i];
        data[i] = data[j];
        data[j] = temp;
    }
   //选择排序
      public static <T extends Comparable<T>>
      void selectionSort(T[] data)
       {
      long startTime=System.nanoTime();
      int min,count= 0;
      T temp;
      for (int index = 0; index < data.length-1; index++)
      {
        count++;
        min = index;
        for (int scan = index+1; scan < data.length; scan++) {
            count++;
            if (data[scan].compareTo(data[min]) < 0)
                min = scan;
        }

        swap(data, min, index);
    }
    long endTime = System.nanoTime();
    System.out.println("选择排序的运行时间为" + (endTime - startTime) + "ns");
    System.out.println("选择排序的比较次数为："+count);

}

    //插入排序
    public static <T extends Comparable<T>>
    void insertionSort(T[] data)
    {
        long startTime=System.nanoTime();
        int count= 0;
        for (int index = 1; index < data.length; index++)
        {
            T key = data[index];
            int position = index;
            while (position > 0 && data[position-1].compareTo(key) > 0)
            {
                data[position] = data[position-1];
                position--;
                count++;
            }
            count++;
            data[position] = key;
        }
        long endTime = System.nanoTime();
        System.out.println("插入排序的程序运行时间为：" + (endTime - startTime) + "ns");
        System.out.println("插入排序的比较次数为："+count);
    }

    //冒泡排序
    public static <T extends Comparable<? super T>> void bubbleSort(T[] data) {
        long startTime = System.nanoTime();
        int position, scan, count = 0;
        for (position = data.length - 1; position >= 0; position--) {
            for (scan = 0; scan < position; scan++) {
                if (data[scan].compareTo(data[scan + 1]) > 0) {
                    swap(data, scan, scan + 1);
                }
                count++;
            }
        }
        long endTime = System.nanoTime();
        System.out.println("冒泡排序的程序运行时间：" + (endTime - startTime) + "ns");
        System.out.println("冒泡排序的比较次数为："+count);
    }

    //快速排序
    public static int count;
    public static long startTime, endTime;
    public static <T extends Comparable<? super T>> void quickSort(T[] data) {
        quickSortRec(data, 0, data.length - 1);
        System.out.println("快速排序的程序运行时间：" + (endTime - startTime) + "ns");
        System.out.println("快速排序的比较次数为："+count);
    }
    private static <T extends Comparable<? super T>> void quickSortRec(T[] data, int min, int max) {
        startTime = System.nanoTime();
        if (min < max) {
            // create partitions
            int indexofpartition = partition(data, min, max);
            // sort the left partition (lower values)
            quickSortRec(data, min, indexofpartition - 1);
            // sort the right partition (higher values)
            quickSortRec(data, indexofpartition + 1, max);
            count++;
        }
        endTime = System.nanoTime();
    }
    private static <T extends Comparable<? super T>> int partition(T[] data, int min, int max) {
        T partitionelement;
        int left, right;
        int middle = (min + max) / 2;

        partitionelement = data[middle];
        swap(data, middle, min);

        left = min;
        right = max;
        while (left < right) {
            while (left < right && data[left].compareTo(partitionelement) <= 0) {
                left++;
                count++;
            }
            while (data[right].compareTo(partitionelement) > 0) {
                right--;
                count++;
            }
            if (left < right)
                swap(data, left, right);
        }
        swap(data, min, right);

        return right;
    }

    //归并排序
    public static int COUNT;
    public static long startTime1, endTime1;
    public static <T extends Comparable<? super T>> void mergeSort(T[] data){
        startTime1 = System.nanoTime();
        mergesort(data,0,data.length-1);
        endTime1 = System.nanoTime();
        System.out.println("归并排序的程序运行时间：" + (endTime1 - startTime1) + "ns");
        System.out.println("归并排序的比较次数为："+ COUNT);
    }
    public static <T extends Comparable<? super T>> void mergesort(T[] data, int min, int max) {
        if (min < max) {
            int mid = (min + max) / 2;
            mergesort(data, min, mid);
            mergesort(data, mid + 1, max);
            merge(data, min, mid, max);
            COUNT++;
        }
    }

    @SuppressWarnings("unchecked")
    private static <T extends Comparable<? super T>> void merge(T[] data, int first, int mid, int last) {
        T[] temp = (T[]) (new Comparable[data.length]);
        int first1 = first, last1 = mid;
        int first2 = mid + 1, last2 = last;
        int index = first1;
        while (first1 <= last1 && first2 <= last2) {
            if (data[first1].compareTo(data[first2]) < 0) {
                temp[index] = data[first1];
                first1++;
            } else {
                temp[index] = data[first2];
                first2++;
            }
            index++;
            COUNT++;
        }
        while (first1 <= last1) {
            temp[index] = data[first1];
            first1++;
            index++;
        }
        while (first2 <= last2) {
            temp[index] = data[first2];
            first2++;
            index++;
        }
        for (index = first; index <= last; index++){
            data[index] = temp[index];
        }
    }


        public static void main (String[]args){
            Integer[] list = {13, 23, 54, 33, 9, 87, 4};
            Integer[] list2 ={13, 23, 54, 33, 9, 87, 4};
            Integer[] list3 = {13, 23, 54, 33, 9, 87, 4};
            Integer[] list4 = {13, 23, 54, 33, 9, 87, 4};
            Integer[] list5 = {13, 23, 54, 33, 9, 87, 4};

            pp9_3.bubbleSort(list);
            for (int num : list)
                System.out.print(num + " ");
            System.out.println();
            System.out.println();


            pp9_3.insertionSort(list2);
            for (int num : list2)
                System.out.print(num + " ");
            System.out.println();
            System.out.println();

            pp9_3.mergeSort(list3);
            for (int num : list3)
                System.out.print(num + " ");
            System.out.println();
            System.out.println();

            pp9_3.quickSort(list4);
            for (int num : list4)
                System.out.print(num + " ");
            System.out.println();
            System.out.println();

            pp9_3.selectionSort(list5);
            for (int num : list5)
                System.out.print(num + " ");

        }

    }



