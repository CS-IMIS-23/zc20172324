package chap12;

import java.util.Iterator;

public class QueueHeapTester
{
    public static void main(String[] args) {
        QueueHeap qp = new QueueHeap();

        qp.enqueue(12);
        qp.enqueue(9);
        qp.enqueue(13);
        qp.enqueue(98);
        qp.enqueue(26);
        qp.enqueue(48);
        System.out.print("入队后的队列为：");
        Iterator iterator = qp.iteratorLevelOrder();
        while(iterator.hasNext())
        {
            System.out.print(iterator.next()+ " ");
        }
        System.out.println();
        System.out.println("用堆表示为：");
        System.out.println(qp);

        qp.dequeue();
        System.out.println("删除队列中一个元素为：");
        Iterator iterator1 = qp.iteratorLevelOrder();
        while(iterator1.hasNext())
        {
            System.out.print(iterator1.next()+ " ");
        }
        System.out.println(qp);
    }
}