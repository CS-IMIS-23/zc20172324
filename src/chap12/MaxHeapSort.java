package chap12;

public class MaxHeapSort
{
    public static void main(String[] args) {
        int[] array ={36,30,18,40,32,45,22,50};
        MaxHeap<Integer> largeArrayHeap = new MaxHeap();
        for (int i = 0;i<array.length;i++)
        {
            largeArrayHeap.addElement(array[i]);
        }
        System.out.println("根据关键字序列：36,30,18,40,32,45,22,50，利用数组构造一颗大顶堆");
        System.out.print("输出构造好的大顶堆序列（层序）:");
        System.out.println(largeArrayHeap);
       // System.out.print("并排序：");
        System.out.println("每轮排序中数组的结果");
        for (int i = 0;i<array.length;i++)
        {
            array[i] = largeArrayHeap.removeMax();
            System.out.println(largeArrayHeap);
        }
        System.out.println("降序排列，排序结果为：");
        for (int i= 0;i<array.length;i++)
            System.out.print(array[i]+" ");


    }


}
