package chap12;
import chap10.LinkedBinaryTree;

/**c
 * HeapSort sorts a given array of Comparable objects using a heap.
 * 
 * @author Lewis and Chase
 * @version 4.0
 */
public class HeapSort<T extends Comparable<T>> {
	/**
	 * Sorts the specified array using a Heap
	 * 
	 * @param data
	 *            the data to be added to the heapsort
	 */
	public void HeapSort(T[] data) {
		ArrayHeap<T> temp = new ArrayHeap<T>();

		// copy the array into a heap
		for (int i = 0; i < data.length; i++)
			temp.addElement(data[i]);

		// place the sorted elements back into the array
		int count =  data.length;
		while (!(temp.isEmpty())) {
			data[count] = temp.removeMax();
			count--;
		}
	}
	
	public static void main(String[] args) {
		Integer[] data = {36,30,18,40,32,45,22,50};
		HeapSort hs = new HeapSort<Integer>();
		hs.HeapSort(data);
		for(int i=0;i<data.length;i++){
			System.out.print(data[i]+" ");
		}
		
	}



}
