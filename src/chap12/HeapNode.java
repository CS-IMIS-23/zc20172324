package chap12;
/** 
 * @author LbZhang
 * @version 创建时间：2015年11月30日 下午8:33:13 
 * @description 堆的结点
 */
public class HeapNode<T > extends BinaryTreeNode<T> {
	
	protected HeapNode<T>  parent;
	protected HeapNode<T>  root;

	public HeapNode(T obj) {
		super(obj);
		parent = null;
	}
	
	public HeapNode<T> getParent(){
		return parent;
	}
	
	public void setElement(T obj){
		element=obj;
	}

	public void setParent(HeapNode<T> node){
		parent = node;
	}
	public HeapNode<T> getNewLastNode(HeapNode<T> last)
	{
		HeapNode<T> result = last;

		while ((result.parent != null) && (result.parent.left == result))
			result = result.parent;

		if (result.parent != null)
			result = (HeapNode<T>) result.parent.left;

		while (result.right != null)
			result = (HeapNode<T>) result.right;

		return result;
	}


}
