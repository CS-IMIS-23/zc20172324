package 实验3;

import chap10.BinaryTreeNode;
import chap9.Hash;
import chap9.LinearNode;
import 实验2.LinkedBinarySearchTree;

import java.util.ArrayList;

public class Searching<T> {
    //线性查找
    public static Comparable linearSearch(Comparable[] list, Comparable target)
    {
        int index = 0;
        boolean found = false;

        while (!found && index < list.length)
        {
            if (list[index].equals(target))
                found = true;
            else
                index++;
        }

        if (found)
            return list[index];
        else
            return null;
    }

    //顺序查找
    public static <T extends Comparable<T>> boolean SequenceSearch(T[] data, int min, int max, T target) {
        int index = min;
        boolean found = false;
        while (!found && index <= max) {
            found = data[index].equals(target);
            index++;
        }
        return found;
    }
    //二分查找
    public static <T extends Comparable<? super T>>
    boolean BinarySearch(T[] data, int min, int max, T target)
    {
        boolean found = false;
        int midpoint = (min + max) / 2;  // determine the midpoint

        if (data[midpoint].compareTo(target) == 0)
            found = true;

        else if (data[midpoint].compareTo(target) > 0)
        {
            if (min <= midpoint - 1)
                found = BinarySearch(data, min, midpoint - 1, target);
        }

        else if (midpoint + 1 <= max)
            found = BinarySearch(data, midpoint + 1, max, target);

        return found;
    }
    //插值查找
    public static int InsertionSearch(int[] a, int value, int low, int high) {
        int mid = low + (value - a[low]) / (a[high] - a[low]) * (high - low);
        if (a[mid] == value)
            return mid;
        if (a[mid] > value)
            return InsertionSearch(a, value, low, mid - 1);
        else
            return InsertionSearch(a, value, mid + 1, high);
    }

    //斐波那契查找
    public static boolean FibonacciSearch(int[] table, int keyWord) {
        //确定需要的斐波那契数
        int i = 0;
        while (getFibonacci(i) - 1 == table.length) {
            i++;
        }
        //开始查找
        int low = 0;
        int height = table.length - 1;
        while (low <= height) {
            int mid = low + getFibonacci(i - 1);
            if (table[mid] == keyWord) {
                return true;
            } else if (table[mid] > keyWord) {
                height = mid - 1;
                i--;
            } else if (table[mid] < keyWord) {
                low = mid + 1;
                i -= 2;
            }
        }
        return false;
    }
    //得到第n个斐波那契数
    public static int getFibonacci(int n) {
        int res = 0;
        if (n == 0) {
            res = 0;
        } else if (n == 1) {
            res = 1;
        } else {
            int first = 0;
            int second = 1;
            for (int i = 2; i <= n; i++) {
                res = first + second;
                first = second;
                second = res;
            }
        }
        return res;
    }

    //树表查找
    public static boolean BinaryTreeSearch(Comparable [] data, Comparable target){
        LinkedBinarySearchTree tree = new LinkedBinarySearchTree();
        for (int a = 0; a< data.length; a++){
            tree.addElement(data[a]);
        }
        if (tree.find(target) == target)
            return true;
        else
            return false;
    }

    //分块查找
    public static boolean BlockSearch(Comparable[] data ,Comparable target,int lump){
        if(target == blockSearch(data,target,lump))
            return true;
        else
            return false;
    }
    private static Comparable blockSearch(Comparable[] data ,Comparable target,int lump){
        int temp = data.length / lump;
        Comparable result = null;
        Comparable[] tp= new Comparable [temp];
        for (int a = 0;a< data.length; a = a+ temp){
            if (data[a].compareTo(target) > 0){
                int c = 0;
                for (int j =a - temp; j < a; j++){
                    tp[c] = data[j];
                    c++;
                }
                result = linearSearch(tp,target);
                break;
            }
            else if (data[a].compareTo(target) == 0){
                result = data[a];
                break;
            }
        }
        return result;
    }

    //哈希算法
    public static <T extends Comparable<? super T>>
    boolean HashSearch(int[] data, int min, int max, int target)
    {
        boolean result = false;
        Hash hashConflict = new Hash();
        for (int i=0;i<data.length;i++)
            hashConflict.ReceiveNum(data[i]);
        int index = target%hashConflict.num;
        while (hashConflict.list[index]!=null&&result==false)
        {
            if (hashConflict.list[index].getElement().equals(target))
                result = true;
            else
                hashConflict.list[index] = hashConflict.list[index].getNext();
        }
        return result;
    }
}




