package 实验3;

import junit.framework.TestCase;
public class EXP1testTest extends TestCase  {
    public void testLinearSearch() throws Exception {
        //search
//        Comparable lin1[] = {0, 1, 2, 3, 9,2324};  //正常
//        assertEquals(true, EXP1test.linearSearch(lin1, 0,5,2));  //正常情况1
//        assertEquals(false, EXP1test.linearSearch(lin1, 0,5,8));  //正常情况2
//
//        Comparable lin2[] = {1, 2, 9,23,24};  //正序
//        assertEquals(true, EXP1test.linearSearch(lin2, 0,4,9));  //正常情况3
//        assertEquals(false, EXP1test.linearSearch(lin2, 0,4,66));  //正常情况4
//
//        Comparable lin3[] = {24, 23, 9,2,1};  //逆序
//        assertEquals(true, EXP1test.linearSearch(lin3, 0,4,9));  //正常情况5
//        assertEquals(false, EXP1test.linearSearch(lin3, 0,4,8));  //正常情况6
//
//        Comparable lin4[] = {"B", "C","D","A","I"};  //正常
//        assertEquals(true, EXP1test.linearSearch(lin4, 0,4,"D"));  //正常情况7
//        assertEquals(false, EXP1test.linearSearch(lin4, 0,4,"Z"));  //正常情况8
//
//         Comparable lin5[] = new Comparable[6];  //空指针异常9
//        assertEquals(true,EXP1test.linearSearch(lin5,0,6,8));
//
//        Comparable  lin6[] = new Comparable[2];//空指针异常10
//        assertEquals(null,EXP1test.linearSearch(lin6,0,2,9));

        Comparable lin7[] = {"A", "C", "E", "G", "Z"};  //边界11
        assertEquals(true,EXP1test.linearSearch(lin7,0,4,"A"));

        Comparable lin8[] = {"A", "C", "E", "G", "Z"};  //边界12
        assertEquals(true,EXP1test.linearSearch(lin8,0,4,"Z"));

        //selectsort
        Comparable[] arr1 = {1,5,13,23,24}; //正序情况1
        EXP1test a =new EXP1test();
        assertEquals("[1, 5, 13, 23, 24]",a.selectionSort(arr1));

        Comparable[] arr2 = {3,6,9,12,15}; //正序情况2
        EXP1test b =new EXP1test();
        assertEquals("[3, 6, 9, 12, 15]",b.selectionSort(arr2));


        Comparable[] arr3 = {24,23,13,5,1}; //逆序情况3
        EXP1test c =new EXP1test();
        assertEquals("[1, 5, 13, 23, 24]",c.selectionSort(arr3));


        Comparable[] arr4 = {15,12,9,6,3}; //逆序情况4
        EXP1test d =new EXP1test();
        assertEquals("[3, 6, 9, 12, 15]",d.selectionSort(arr4));

        Comparable[] arr5 = {1,13,5,24,23}; //正常情况5
        EXP1test e =new EXP1test();
        assertEquals("[1, 5, 13, 23, 24]",e.selectionSort(arr5));

        Comparable[] arr6 = {1,43,2,88,23}; //正常情况6
        EXP1test f =new EXP1test();
        assertEquals("[1, 2, 23, 43, 88]",f.selectionSort(arr6));

        Comparable[] arr7 = {55,13,5,24,66}; //正常情况7
        EXP1test g =new EXP1test();
        assertEquals("[5, 13, 24, 55, 66]",g.selectionSort(arr7));

        Comparable[] arr8 = {33,43,2,3,23}; //正常情况8
        EXP1test h =new EXP1test();
        assertEquals("[2, 3, 23, 33, 43]",h.selectionSort(arr8));


        Comparable[] arr9 = new Comparable[1];//9
        EXP1test i =new EXP1test();
        assertEquals("[null]",i.selectionSort(arr9));

        Comparable[] arr10 = new Comparable[2]; //空指针异常10
        EXP1test j =new EXP1test();
        assertEquals("[null]",j.selectionSort(arr10));



    }

}
