package 实验3;

import chap9.Hash;

public class SearchingTest<T> {
    public static void main(String[] args) {
        //顺序查找
        String[] string = {"20","7","12","66"};
        System.out.print("原序列如下：");
        for (Comparable i : string)
            System.out.print(i + " ");
        System.out.println();
        System.out.println("顺序查找是否含有‘66’：" + Searching.SequenceSearch(string,0,string.length,"66"));
        System.out.println();
        //二分查找
        String[] int1 = {"1","5","10","15","20","25"};
        System.out.print("原序列如下：");
        for (Comparable i : int1)
            System.out.print(i + " ");
        System.out.println();
        System.out.println("二分查找是否含有‘25’：" + Searching.BinarySearch(int1,0,5,"25"));
        System.out.println();
        //插值查找
        int[] int2  = {13, 19, 27, 26, 28, 30, 38 };
        System.out.print("原序列如下：");
        for (Comparable i : int2)
            System.out.print(i + " ");
        System.out.println();
        System.out.println("插值查找是否含有‘27’：在索引" + Searching.InsertionSearch(int2,27,0,6) + "位置");
        System.out.println();
        //斐波那契查找
        int[] int3 = {1,4,6,8,23};
        System.out.print("原序列如下：");
        for (Comparable i : int3)
            System.out.print(i + " ");
        System.out.println();
        System.out.println("斐波那契查找是否含有‘33’：" + Searching.FibonacciSearch(int3,33) );
        System.out.println();
        //树表查找
        Comparable[] comparable1= { 11, 22, 33, 44, 55};
        System.out.print("原序列如下：");
        for (Comparable i : comparable1)
            System.out.print(i + " ");
        System.out.println();
        System.out.println("树表查找是否含有‘55’：" + Searching. BinaryTreeSearch(comparable1,55));
        System.out.println();
        //分块查找
        Comparable[] comparable2= { 66, 77, 88, 99};
        System.out.print("原序列如下：");
        for (Comparable i : comparable2)
            System.out.print(i + " ");
        System.out.println();
        System.out.println("分块查找是否含有‘100’：" + Searching. BlockSearch(comparable2,100,2));
        System.out.println();
        //哈希查找
        int[] int4 = {3,32,45,34,7,23,9,87,11,2};
        System.out.print("原序列如下：");
        for (Comparable i : int4)
            System.out.print(i + " ");
        System.out.println();
        System.out.print("哈希查找是否含有元素14： "+Searching.HashSearch(int4,0,int4.length-1,14));
          }
}
