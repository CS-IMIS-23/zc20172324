package 实验3;

public class Sorting {
    //希尔排序
    public static void ShellSort(Comparable[] data) {
        int j = 0, temp = 0, k = 2;  //每个子序列中的元素个数
        for (int increment = data.length / k; increment > 0; increment /= k) {  //增量不断缩小
            for (int i = increment; i < data.length; i++) {
                temp = (int) data[i];
                for (j = i - increment; j >= 0; j -= increment) {  //增量划分子序列
                    if (temp < (int) data[j]) {
                        data[j + increment] = data[j];  //子序列元素互换，选择排序
                    } else {
                        break;
                    }
                }
                data[j + increment] = temp;
            }
        }
    }

    //堆排序
    public static void HeapSort(int[] arr) {
        //1.构建大顶堆
        for (int i = arr.length / 2 - 1; i >= 0; i--) {
            //从第一个非叶子结点从下至上，从右至左调整结构
            adjustHeap(arr, i, arr.length);
        }
        //2.调整堆结构+交换堆顶元素与末尾元素
        for (int j = arr.length - 1; j > 0; j--) {
            swap(arr, 0, j);//将堆顶元素与末尾元素进行交换
            adjustHeap(arr, 0, j);//重新对堆进行调整
        }
    }

    public static void adjustHeap(int[] arr, int i, int length) {
        int temp = arr[i];//先取出当前元素i
        for (int k = i * 2 + 1; k < length; k = k * 2 + 1) {//从i结点的左子结点开始，也就是2i+1处开始
            if (k + 1 < length && arr[k] < arr[k + 1]) {//如果左子结点小于右子结点，k指向右子结点
                k++;
            }
            if (arr[k] > temp) {//如果子节点大于父节点，将子节点值赋给父节点（不用进行交换）
                arr[i] = arr[k];
                i = k;
            } else {
                break;
            }
        }
        arr[i] = temp;//将temp值放到最终的位置
    }

    public static void swap(int[] arr, int a, int b) {
        int temp = arr[a];
        arr[a] = arr[b];
        arr[b] = temp;
    }

    //选择排序
    public static void SelectionSort(Comparable[] data) {
        int min;

        for (int index = 0; index < data.length - 1; index++) {
            min = index;
            for (int scan = index + 1; scan < data.length; scan++)
                if (data[scan].compareTo(data[min]) < 0)
                    min = scan;

            swap(data, min, index);
        }
    }

    private static void swap(Comparable[] data, int index1, int index2) {
        Comparable temp = data[index1];
        data[index1] = data[index2];
        data[index2] = temp;
    }

    //冒泡排序
    public static void BubbleSort(Comparable[] data) {
        int position, scan;

        for (position = data.length - 1; position >= 0; position--) {
            for (scan = 0; scan <= position - 1; scan++)
                if (data[scan].compareTo(data[scan + 1]) > 0)
                    swap(data, scan, scan + 1);
        }
    }



}
