package 实验3;

import chap6.ArrayList;

import java.util.Arrays;

public class EXP1test {
    //线性法查找
    public static <T> boolean linearSearch(T[] data, int min, int max, T target) {
        int index = min;
        boolean found = false;

        while (!found && index <= max) {
            found = data[index].equals(target);
            index++;
        }

        return found;
    }

    //选择排序
    public  <T extends Comparable<T>>
    T selectionSort(T[] data)
    {
        int min;


        for (int index = 0; index < data.length-1; index++)
        {
            min = index;
            for (int scan = index+1; scan < data.length; scan++)
                if (data[scan].compareTo(data[min])<0)
                    min = scan;

            swap(data, min, index);
        }
        return (T) Arrays.toString(data);
    }
    static <T extends Comparable<T>>
    void swap(T[] data, int index1, int index2)
    {
        T temp = data[index1];
        data[index1] = data[index2];
        data[index2] = temp;
    }



}
