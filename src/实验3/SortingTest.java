package 实验3;

import chap11.LinkedBinarySearchTree;

public class SortingTest {
    public static void main(String[] args) {
        //希尔排序
        Comparable[] arr1 = {3, 12, 8, 77, 54, 84, 91, 110, 13};
        System.out.println("\n原数组如下：");
        for (Comparable num1 : arr1)
            System.out.print(num1 + " ");
        Sorting.ShellSort(arr1);
        System.out.println("\n希尔排序如下：");
        for (Comparable num4 : arr1)
            System.out.print(num4 + " ");
        System.out.println();
        //堆排序
        int[] arr2 = {1,2,8,4,2,98,323,11};
        System.out.println("\n原数组如下：");
        for (Comparable num2 : arr2)
            System.out.print(num2 + " ");
        Sorting.HeapSort(arr2);
        System.out.println("\n堆排序如下：");
        for (int num4 : arr2)
            System.out.print(num4 + " ");
        System.out.println();
        //二叉树排序
        LinkedBinarySearchTree tree = new LinkedBinarySearchTree();
        tree.addElement(8);
        tree.addElement(3);
        tree.addElement(65);
        tree.addElement(22);
        tree.addElement(9);
        tree.addElement(84);
        tree.addElement(91);
        tree.addElement(110);
        tree.addElement(12);
        System.out.println("\n二叉树排序如下：");
        for(int i = 0;i < 9;i++) {
            System.out.print(tree.findMin() + " ");
            tree.removeElement(tree.findMin());
        }
        System.out.println();
        //选择排序
        Comparable[] arr3 = {1, 12, 2, 7, 98};
        System.out.println("\n原数组如下：");
        for (Comparable num1 : arr3)
            System.out.print(num1 + " ");
        Sorting.SelectionSort(arr3);
        System.out.println("\n选择排序如下：");
        for (Comparable num2 : arr3)
            System.out.print(num2 + " ");
        System.out.println();
        //冒泡排序
        Comparable[] arr4 = {77,44,55,33};
        System.out.println("\n原数组如下：");
        for (Comparable num1 : arr4)
            System.out.print(num1 + " ");
        Sorting.BubbleSort(arr4);
        System.out.println("\n冒泡排序如下：");
        for (Comparable num5 : arr4)
            System.out.print(num5 + " ");


    }
}