package chap11;

import chap10.ArrayUnorderedList;
import chap10.UnorderedListADT;

public class AVLTree<T extends Comparable<T>> {
    private AVLNode<T> root;

    private int h(AVLNode<T>  x) {
        if(x==null)
            return 0;
        else
            return x.height;
    }
    //左旋
    private AVLNode<T> L(AVLNode<T> x) {
        AVLNode<T> mid=x.left;
        x.left=mid.right;
        mid.right=x;
        x.height=Math.max(h(x.left),h(x.right))+1;
        mid.height=Math.max(mid.left.height,x.height)+1;
        return mid;
    }
    //右旋
    private AVLNode<T> R(AVLNode<T> x){
        AVLNode<T> mid=x.right;
        x.right = mid.left;
        mid.left = x;
        x.height = Math.max(h(x.left),h(x.right))+1;
        mid.height = Math.max(mid.right.height,x.height)+1;
        return mid;
    }
    //左右旋
    private AVLNode<T> LR(AVLNode<T> x) {
        x.left=R(x.left);
        return L(x);
    }
    //右左旋
    private AVLNode<T> RL(AVLNode<T> x){
        x.right=L(x.right);
        return R(x);
    }
    //添加
    public void add(T value) {
        if (value==null){
            throw new RuntimeException("data can\'t not be null ");
        }
        this.root=insert(value,root);
    }
    //添加到树中
    private AVLNode<T> insert(T value, AVLNode<T> x) {
        if(x==null)x=new AVLNode<T>(value);
        else {
            if(value.compareTo(x.val)>0) {
                x.right=insert(value,x.right);
                if(h(x.right)-h(x.left)==2) {
                    if(value.compareTo(x.right.val)>0) {
                        x=R(x);
                    }
                    else {
                        x=RL(x);
                    }
                }
                x.height=Math.max(h(x.left),h(x.right))+1;
            }
            else if(x.val.compareTo(value)>0) {
                x.left=insert(value,x.left);
                if(h(x.right)-h(x.left)==-2) {
                    if(value.compareTo(x.left.val)<0) {
                        x=L(x);
                    }
                    else {
                        x=LR(x);
                    }
                }
                x.height=Math.max(h(x.left),h(x.right))+1;
            }
            else {
                x.height = Math.max( h(x.left), h(x.right) ) + 1;
            }

        }
        return x;
    }
    public void remove(T value) {
        if(value==null)throw new NullPointerException("No Such Value");
        delete(value,root);
    }
    private AVLNode<T> delete(T value,AVLNode<T> x) {
        if(x==null)return null;
        if(value.compareTo(x.val)>0) {
            x.right=delete(value,x.right);
            if(h(x.left)-h(x.right)==2) {
                if(x.left.left!=null) {
                    x=L(x);
                }
                else {
                    x=LR(x);
                }
            }
        }
        else if(value.compareTo(x.val)<0){
            x.left=delete(value,x.left);
            if(h(x.right)-h(x.left)==2) {
                if(x.right.right!=null) {
                    x=R(x);
                }
                else {
                    x=RL(x);
                }
            }
        }
        else if(x.right!=null){
            x.val=min(x.right);
            x.right=delete(x.val,x.right);
        }
        else {
            x=x.left;
        }
        if(x!=null)
            x.height=Math.max(h(x.left),h(x.right))+1;
        return x;
    }
    private T min(AVLNode<T> x) {
        T ans;
        if(x.left!=null) {
            ans=min(x.left);
        }
        else {
            T temp=x.val;
            if(x.right!=null) {
                x=x.right;
            }
            else
                x=null;
            ans=temp;
        }
        return ans;
    }
    public AVLTree(){
        root=null;
    }
    public void print() {
        if(root==null)
            throw new NullPointerException();
        else {
            ThoughBinaryTree(root);
        }
    }
    private void ThoughBinaryTree(AVLNode<T> x) {
        if(x==null)
            return;
        else {
            System.out.print(x.val + "\t");
            ThoughBinaryTree(x.left);
            ThoughBinaryTree(x.right);
        }
    }
    public int getHeight() {
        return height(root);
    }

    private int height(AVLNode<T> node) {
        if(node==null){
            return 0;
        }
        int hl = height(node.getLeft());
        int hr = height(node.getRight());
        if (hl > hr)
            return ++hl;
        else
            return ++hr;
    }
    public String printTree2() {
        UnorderedListADT<AVLNode<T>> nodes = new ArrayUnorderedList<AVLNode<T>>();
        UnorderedListADT<Integer> levelList = new ArrayUnorderedList<Integer>();

        AVLNode<T> current;
        String result = "";
        int printDepth =getHeight() ;//树的高度
        int possibleNodes = (int) Math.pow(2, printDepth + 1);//由于根节点位置是0，最下层是第printDepth+1层，这个二叉树可能的总结点数就是2的printDepth+1次方
        int countNodes = 0;//结点数目

        nodes.addToRear(root);//在无序列表中添加根部结点
        Integer currentLevel = 0;//当前层
        Integer previousLevel = -1;//前一层
        levelList.addToRear(currentLevel);//把当前层添加到层数链表中

        while (countNodes < possibleNodes) {//此树不满时
            countNodes = countNodes + 1;//从0开始加
            current = nodes.removeFirst();//把第一个数取出来
            currentLevel = levelList.removeFirst();//设置成当前层数
            if (currentLevel > previousLevel) {//如果不在同一层就换行
                result = result + "\n\n";//进行换行
                previousLevel = currentLevel;//层数下移
                for (int j = 0; j < ((Math.pow(2, (printDepth - currentLevel))) - 1); j++)
                    result = result + " ";//每一层加空格，第n层有2^(n-1)个数
            } else {//如果在同一行，每两个数之间加空格
                for (int i = 0; i < (Math.pow(2, (printDepth - currentLevel + 1)) - 1); i++) {
                    result = result + " ";
                }
            }
            if (current != null) {//如果当前结点不为空，记录他的左右孩子，并增加层数
                result = result + (current.val).toString();
                nodes.addToRear(current.left);
                levelList.addToRear(currentLevel + 1);
                nodes.addToRear(current.right);
                levelList.addToRear(currentLevel + 1);
            } else {//如果当前结点为空，给他分配空间，存一个null进去
                nodes.addToRear(null);
                levelList.addToRear(currentLevel + 1);
                nodes.addToRear(null);
                levelList.addToRear(currentLevel + 1);
                result = result + " ";
            }
        }

        return result;}
}

