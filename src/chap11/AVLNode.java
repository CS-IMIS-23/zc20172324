package chap11;
public class AVLNode<T  extends Comparable<T>> {
    public T val;
    public int height;
    public AVLNode<T> left,right;

    public AVLNode(T data) {
        this(null,null,data);
    }

    public AVLNode(AVLNode<T> left, AVLNode<T> right, T data) {
        this(left,right,data,0);
    }

    public AVLNode(AVLNode<T> left, AVLNode<T> right, T data, int height) {
        this.left=left;
        this.right=right;
        this.val=data;
        this.height = 0;
    }
    /**
     * Returns the number of non-null children of this node.
     * @return the integer number of non-null children of this node
     * 使用递归的方式
     */
    public int numChildren() {
        int children = 0;

        if (left != null)
            children = 1 + left.numChildren();

        if (right != null)
            children = children + 1 + right.numChildren();

        return children;
    }

    /**
     * Return the element at this node.
     * @return the element stored at this node
     */
    public T getElement() {
        return val;
    }

    public AVLNode<T> getRight() {
        return right;
    }

    public void setRight(AVLNode<T> node) {
        right = node;
    }

    public AVLNode<T> getLeft() {
        return left;
    }

    public void setLeft(AVLNode<T> node) {
        left = node;
    }

}
//
//public class AVLTreeNode<T extends Comparable<T>> {
//    public int height;
//    T node;
//    AVLTreeNode<T> left;
//    AVLTreeNode<T> right;
//
//    public AVLTreeNode(T obj) {
//        this.node = obj;
//        this.left = null;
//        this.right = null;
//        this.height=0;
//    }
//
//    public AVLTreeNode(T node, AVLTreeNode<T> left, AVLTreeNode<T> right) {
//        this.node = node;
//        this.left = left;
//        this.right = right;
//        this.height=0;
//    }
//
//
//    public boolean isLeaf(){
//        if(left==null && right==null){
//            return true;
//        }else {
//            return false;
//        }
//    }
//    public AVLTreeNode(T obj, AVLTree<T> left, AVLTree<T> right) {
//        node = obj;
//        if (left == null)
//            this.left = null;
//        else
//            this.left = left.getRootNode();
//
//        if (right == null)
//            this.right = null;
//        else
//            this.right = right.getRootNode();
//    }
//
//    /**
//     * Returns the number of non-null children of this node.
//     * @return the integer number of non-null children of this node
//     * 使用递归的方式
//     */
//    public int numChildren() {
//        int children = 0;
//
//        if (left != null)
//            children = 1 + left.numChildren();
//
//        if (right != null)
//            children = children + 1 + right.numChildren();
//
//        return children;
//    }
//
//    /**
//     * Return the element at this node.
//     * @return the element stored at this node
//     */
//    public T getElement() {
//        return node;
//    }
//
//    public AVLTreeNode<T> getRight() {
//        return right;
//    }
//
//    public void setRight(AVLTreeNode<T> node) {
//        right = node;
//    }
//
//    public AVLTreeNode<T> getLeft() {
//        return left;
//    }
//
//    public void setLeft(AVLTreeNode<T> node) {
//        left = node;
//    }
//
//}