package database;

import java.sql.*;
import java.util.Scanner;

import static java.lang.System.out;

public class World {
    public static void main (String args[])
    {
        Connection conn = null;
        Scanner console = new Scanner(System.in);
        try
        {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection("jdbc:mysql://localhost/" +
                    "world?user=root&password=");

            // Check the connection
            if (conn != null) {
                out.println("输入以下下标选择查询类型：");
                out.println("1.查询世界上超过6017232的所有城市列表");
                out.println("2.查询哪个国家的平均寿命最长");
                out.println("3.查询哪个国家的平均寿命最短");
                out.println("4.查询世界上的所有中东国家的总人口");
                switch (Integer.parseInt(console.nextLine())) {
                    case 1:
                        System.out.println();
                        Statement stmt = conn.createStatement();
                        System.out.println("查询世界上超过601723的所有城市列表");
                        ResultSet result = stmt.executeQuery("SELECT * FROM city WHERE city.Population>6017232");
                        World.showResults("city", result);
                        break;

                    case 2:
                        System.out.println();
                        System.out.println("哪个国家的平均寿命最长");
                        Statement stmt1 = conn.createStatement();
                        ResultSet result1 = stmt1.executeQuery("SELECT * FROM country WHERE country.LifeExpectancy=(SELECT MAX(country.LifeExpectancy)FROM country)");
                        World.showResults("country", result1);
                        break;

                    case 3:
                        System.out.println();
                        System.out.println("哪个国家的平均寿命最短");
                        Statement stmt2 = conn.createStatement();
                        ResultSet result2 = stmt2.executeQuery("SELECT * FROM country WHERE country.LifeExpectancy=(SELECT MIN(country.LifeExpectancy)FROM country) ");
                        World.showResults("country", result2);
                        break;

                    case 4:
                        System.out.println();
                        System.out.println("查询世界上的所有中东国家的总人口");
                        Statement stmt3 = conn.createStatement();
                        ResultSet result3 = stmt3.executeQuery("SELECT SUM(Population) FROM country WHERE country.Region=\"Middle East\"");
                        World.showResults("country", result3);
                        break;

                }

                conn.close();
            }
        } catch (SQLException ex) {
            System.out.println("SQLException: " + ex.getMessage());
            ex.printStackTrace();

        } catch (Exception ex) {
            System.out.println("Exception: " + ex.getMessage());
            ex.printStackTrace();
        }
    }



    //-----------------------------------------------------------------
    //  Displays the contents of the specified ResultSet.
    //-----------------------------------------------------------------
    public static void showResults(String tableName, ResultSet rSet)
    {
        try
        {
            ResultSetMetaData rsmd = rSet.getMetaData();
            int numColumns = rsmd.getColumnCount();
            String resultString = null;
            if (numColumns > 0)
            {
                resultString = "\nTable: " + tableName + "\n" +
                        "=======================================================\n";
                for (int colNum = 1; colNum <= numColumns; colNum++)
                    resultString += rsmd.getColumnLabel(colNum) + "     ";
            }
            System.out.println(resultString);
            System.out.println(
                    "=======================================================");

            while (rSet.next())
            {
                resultString = "";
                for (int colNum = 1; colNum <= numColumns; colNum++)
                {
                    String column = rSet.getString(colNum);
                    if (column != null)
                        resultString += column + "     ";
                }
                System.out.println(resultString + '\n' +
                        "-------------------------------------------------------");
            }
        } catch (SQLException ex) {
            System.out.println("SQLException: " + ex.getMessage());
            ex.printStackTrace();
        }
    }
}