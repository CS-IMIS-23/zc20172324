package chap5;

public interface Deque<T> {
    //头插
    public void Headenqueue(T element);
    //尾插
    public void Lastenqueue(T element);
    //头删
    public T Headdequeue();
    //尾删
    public T Lastdequeue();
    //查看头部元素
    public T first();
    //查看尾部元素
    public T last();



    public boolean isEmpty();
    public int size();
    public String  toString();



}