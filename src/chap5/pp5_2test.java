package chap5;

public class pp5_2test {
    public static void main(String[] args) {
        pp5_2 a = new pp5_2();
        a.enqueue(2);
        a.enqueue(7);
        a.enqueue(3);
        a.enqueue(8);
        a.enqueue(1);
        a.enqueue(1);

        System.out.println("环形队列："+a.toString());
        System.out.println("出列元素："+a.dequeue());
        System.out.println("首个元素："+a.first());
        System.out.println("容量："+a.size());
        System.out.println("是否为空："+a.isEmpty());
        System.out.println("环形队列："+a.toString());

    }
}
