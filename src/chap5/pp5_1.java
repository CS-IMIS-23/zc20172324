package chap5;

import chap4.LinearNode;
import chap3.EmptyCollectionException;

public class pp5_1<T> implements QueueADT<T> {
    private int count;


    private LinearNode<T> head, tail;

    public pp5_1() {
        count = 0;
        head = tail = null;
    }

    @Override
    public void enqueue(T element) {
        LinearNode<T> node = new LinearNode<T>(element);

        if (isEmpty())
            head = node;
        else
            tail.setNext(node);
        tail = node;
        count++;
    }

    @Override
    public T dequeue() throws EmptyCollectionException {
        if (isEmpty())
            throw new EmptyCollectionException("queue");

        T result = head.getElement();
        head = head.getNext();
        count--;

        if ((isEmpty()))
            tail = null;

        return result;
    }

    @Override
    public T first() {
        return head.getElement();
    }

    @Override
    public boolean isEmpty() {
        if(count!=0)
            return false;
        else
            return true;
    }
    @Override
    public int size() {
        return count;
    }

    public String toString(){
        String result = "";

        for(LinearNode current = this.head; current != null; current = current.getNext()) {
            result = result + current.getElement() + "\n";
        }
        return result;
    }
}
