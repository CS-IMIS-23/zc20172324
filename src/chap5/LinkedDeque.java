package chap5;

import chap3.EmptyCollectionException;


public class LinkedDeque<T> implements Deque<T> {

    private int count;
    private QueueNode<T> front, rear;

    public LinkedDeque() {
        count = 0;
        front = null;
        rear = null;
    }

    //头插
    @Override
    public void Headenqueue(T element) {
        QueueNode<T> node = new QueueNode<T>(element);

        node.setNext(front);
        if(front==null){
            rear = node;
        }else {
            front.setPrevious(node);
        }
        front = node;
        count++;
    }

    //尾插,链表可以进行尾巴插入--输出顺序和输入顺序一致
    @Override
    public void Lastenqueue(T element) {
        QueueNode<T> node = new QueueNode<T>(element);
        rear.setNext(node);
        node.setPrevious(rear);
        rear = node;
        count++;


    }
    //头删
    @Override
    public T Headdequeue() throws EmptyCollectionException {
        if (isEmpty())
            throw new EmptyCollectionException("Queue is empty.");
        QueueNode<T> temp = front;
        front = front.getNext();
        temp.setPrevious(null);
        if (front!=null)
            front.setPrevious(null);
        count--;
        return  temp.getElement();


    }

    //尾删
    @Override
    public T Lastdequeue() throws EmptyCollectionException {
        if (isEmpty())
            throw new EmptyCollectionException("Queue is empty.");
        QueueNode<T> temp = rear;
        rear = rear.getPrevious();
        rear.setNext(null);
        temp.setNext(null);
        count--;
        return temp.getElement();
    }

    //查看头部元素
    @Override
    public T first() throws EmptyCollectionException {
        if (isEmpty())
            throw new EmptyCollectionException("Queue is empty.");

        return front.getElement();
    }

    //查看尾部元素
    @Override
    public T last() throws EmptyCollectionException {
        if (isEmpty())
            throw new EmptyCollectionException("Queue is empty.");

        return rear.getElement();
    }

    @Override
    public int size() {
        return count;
    }

    @Override
    public boolean isEmpty() {
        if (count == 0)
            return true;
        else
            return false;
    }

    public String toString() {
        String result = "";
        QueueNode<T> current = front;
        for (int a = 0; a < count; a++) {
            result += current.getElement() + " ";
            current = current.getNext();
        }
        return result;
    }


}

