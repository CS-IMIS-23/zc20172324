package chap5;

public class pp5_1test {
    public static void main(String[] args) {
        pp5_1 a = new pp5_1();
        a.enqueue(1);
        a.enqueue(2);
        a.enqueue(3);
        a.enqueue(7);

        System.out.println("链表："+a.toString());
        System.out.println("弹出："+a.dequeue());
        System.out.println("顶："+a.first());
        System.out.println("空否："+a.isEmpty());
        System.out.println("容量："+a.size());
        System.out.println("链表："+a.toString());
    }
}
