package chap5;
import chap3.EmptyCollectionException;
import chap4.LinearNode;


public class pp5_2<T> implements QueueADT<T> {
    private final int DEFAULT_CAPACITY = 100;
    private int front, rear, count;
    private T[] queue;

    public pp5_2(int initialCapacity){
        front = rear =count = 0;
        queue = ((T[])(new Object[initialCapacity]));

    }

    public pp5_2()
    {
        this (100);
    }

    @Override
    public void enqueue(T element) {
        if(size() == queue.length)
            expandCapacity();

        queue[rear] = element;
        rear = (rear+1) % queue.length;

        count++;
    }

    private void expandCapacity() {
        T[] larger = (T[]) (new Object[queue.length*2]);
        for(int scan = 0;scan <count; scan++){
            larger[scan] = queue[front];
            front = (front + 1) % queue.length;
        }
        front = 0;
        rear = count;
        queue = larger;
    }


    @Override
    public T dequeue() throws EmptyCollectionException {
        if(isEmpty())
            throw new EmptyCollectionException("queue");

        T result = queue[front];
        queue[rear] = null;
        front = (front + 1) % queue.length;

        count--;
        return result;

    }

    @Override
    public T first() {
        return queue[front];
    }

    @Override
    public boolean isEmpty() {
        if(count!=0)
            return false;
        else
            return true;
    }

    @Override
    public int size() {
        return count;
    }

    public String toString(){
        String result = "";
        int a = front;
        if (front != queue.length) {
            for (int i = front; i < count+a; i++) {
                result += queue[i]+" ";
            }
            if (front == queue.length) {
                for (int i = 0; i < rear; i++) {
                    result += queue[i]+" ";
                }
            }
        }
        return result;

    }





}

