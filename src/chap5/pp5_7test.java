package chap5;

public class pp5_7test {
    public static void main(String[] args) {
        LinkedDeque a  = new LinkedDeque();
        a.Headenqueue(1);
        a.Lastenqueue(5);
        a.Headenqueue(2);
        a.Lastenqueue(4);

        System.out.println("队列前端："+a.first());
        System.out.println("队列后端："+a.last());
        System.out.println("队列尺寸："+a.size());
        System.out.println("队列是否为空："+a.isEmpty());

        System.out.println("队列："+a.toString());
        System.out.println("队列前删："+a.Headdequeue());
        System.out.println("队列后删："+a.Lastdequeue());
        System.out.println("队列："+a.toString());
    }
}
