package chap5;

import java.util.LinkedList;
import java.util.Queue;

public class Codes {
    public static void main(String[] args) {
        //encode and decode a message using a key of value stored in a queue
        int[] key = {5, 12, -3, 8, -9, 4, 10};
        Integer keyValue;
        String encoded = "",decoded = "";
        String message = "All programmers are playWrights and all" +
                " computers are lousy actors";


        Queue<Integer> encodingQueue = new LinkedList<>();
        Queue<Integer> decodingQueue = new LinkedList<>();

        /** load key queue **/
        for(int scan = 0; scan < key.length; scan++){
            encodingQueue.add(key[scan]);
            decodingQueue.add(key[scan]);
        }

        /** encode message **/
        for(int scan = 0; scan < message.length(); scan++){
            //取一个密钥
            keyValue = encodingQueue.remove();
            //会将该字符移动Unicode字符集的另外一个位置
            encoded += (char)(message.charAt(scan) + keyValue);
            //将密钥重新存储到队列中
            encodingQueue.add(keyValue);
        }
        System.out.println("Encoded Message:\n"+encoded+"\n");

        /** decode message **/
        for(int scan = 0; scan < encoded.length(); scan++){
            keyValue = decodingQueue.remove();
            decoded += (char)(encoded.charAt(scan) - keyValue);
            decodingQueue.add(keyValue);
        }
        System.out.println("Decoded Message:\n"+decoded);

    }
}