package chap5;

public class QueueNode<T> {
    private QueueNode<T> next, previous;
    private T element;

    public QueueNode(){
        next = null;
        element = null;
        previous = null;
    }

    public QueueNode(T elem){
        next = null;
        element = elem;
    }

    public QueueNode<T> getNext(){
        return next;
    }

    public QueueNode<T> getPrevious(){
        return previous;
    }

    public T getElement(){
        return element ;
    }

    public void setNext(QueueNode<T> node){
        next = node;
    }

    public void setPrevious(QueueNode<T> node){
        previous = node;
    }

    public void setElement(T elem){
        element = elem;
    }
}