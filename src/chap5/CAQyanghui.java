package chap5;

import chap3.EmptyCollectionException;


public class CAQyanghui<T> implements QueueADT<T> {
    private final int DEFAULT_CAPACITY = 100;
    private int front, rear, count;
    private T[] queue;

    public CAQyanghui(int initialCapacity){
        front = rear =count = 0;
        queue = ((T[])(new Object[initialCapacity]));

    }

    public CAQyanghui()
    {
        this (100);
    }

    @Override
    public void enqueue(T element) {
        if(size() == queue.length)
            expandCapacity();

        queue[rear] = element;
        rear = (rear+1) % queue.length;

        count++;

    }

    private void expandCapacity() {
        T[] larger = (T[]) (new Object[queue.length*2]);
        for(int scan = 0;scan <count; scan++){
            larger[scan] = queue[front];
            front = (front + 1) % queue.length;
        }
        front = 0;
        rear = count;
        queue = larger;
    }


    @Override
    public T dequeue() throws EmptyCollectionException {
        if(isEmpty())
            throw new EmptyCollectionException("queue");

        T result = queue[front];
        queue[rear] = null;
        front = (front + 1) % queue.length;

        count--;
        return result;

    }

    @Override
    public T first() throws EmptyCollectionException {
        if(isEmpty())
            throw new EmptyCollectionException("queue");
        T result = queue[front];
        return result;
    }

    @Override
    public boolean isEmpty() {
        if (size() == 0) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public int size() {
        int result = rear-front;
        return result;
    }

    public String toString(){
//        String result = "";
//
//        for(int i = 0;i<size();i++){
//            result +=String.valueOf(queue1[i]);
//        }
//        return result;
        String result ="";
        int temp = front;
        for(int i = 0;i < count; i++) {
            result += queue[temp] + " ";
            temp = (temp + 1) % queue.length;
        }
        return result;
    }


}
