
public class Movies
{
    public static void main(String[] args)
    {
        DVD2[] movies=  new DVD2[7];

        movies[0]=new DVD2("The Godfather", "Francis Ford Coppola", 1972,24.95,true);
        movies[1]=new DVD2("District 9", "Neill Blomkamp", 2009,19.95,false);
        movies[2]=new DVD2("Iron Man","Jon Favreau", 2008,15.95,false);
        movies[3]=new DVD2("All About Eve", "Joseph Mankiewicz", 1950,17.50,false);
        movies[4]=new DVD2("The Matrix", "Andy & Lana Wachowski", 1999,19.95,true);
        movies[5]=new DVD2("Iron Man 2","Jon Favreau", 2010,22.99,false);
        movies[6]=new DVD2("Casablanca","Michael Curtiz", 1942,19.95,false);

        System.out.println("递增排列： ");
        Sorting.selectionSort(movies);

        for(DVD2 movie:movies)
            System.out.println(movie);

    }
}


