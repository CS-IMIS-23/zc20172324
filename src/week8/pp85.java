
//编写一个程序，计算并输出一组整数x1~xn的平均值和方差
//输入不超过50个值，以浮点数计算mean和sd。
import java.util.Scanner;

public class pp85 {
    public static void main(String[] args)
    {


        int number;
        int i = 0;

        String another = "y";
        int[] list = new int[50];
        //创建一个可以连续输入数字的循环
        while(another.equalsIgnoreCase("y"))//哦~这里的another要和下面输入的another做比较
        {
            Scanner scan = new Scanner(System.in);
            System.out.print("是否继续输入（y/n）： ");
            another = scan.nextLine();//就是这里的


            if (another.equalsIgnoreCase("y") ) {
                System.out.print("Enter your number: ");
                number = scan.nextInt();

                list[i] = number;
                i++;
            }
        }

        //计算平均值
        int count = 0;
        for(int I = 0;I<=i;I++)
            count += list[I];

        double mean ;
        mean = count/i;
        System.out.println("mean= "+mean);

        //计算标准方法
        double sdd =0;
        for(int II =0;II<=i;II++)
            sdd += (list[II]-mean)*(list[II]-mean);


        double sd;
        sd = Math.sqrt(sdd);
        System.out.println("sd = "+sd);















    }
}
