
import java.text.NumberFormat;

//英航最多可以处理30个拥有储存账户的用户，编写一个程序用于管理这些账号
//随时保持更新账户信息，并使每一个用户可以存取款，当遇到无效交易时，生成适当错误信息
//可以用account类，增减3%的利息。
public class Accounter
{
    private  double RATE = 0.030;  // 银行利息是3%

    private long acctNumber;
    private double balance;
    private String name;
    private double withdrawal;

    //-----------------------------------
    // Sets up the account by defining its owner, account number,
    // and initial balance
    //---------------------------------
    public Accounter(String owner,long account, double initial,double withdrawal)
    {
        name = owner;
        acctNumber = account;
        balance = initial;
        this.withdrawal = withdrawal;
    }




    //-------------------------------------
    // Deposits the specified amount into the account. Returns the
    // new balance.
    //-------------------------------------------
    public double deposit(double amount)
    {
        balance = balance + amount;
        return balance;
    }

    //----------------------------------------
    // Withdraw the specified amount from the account and applies
    // the fee . Returns the new balance.
    //--------------------------------------------------
    public double withdraw(double amount, double fee)
    {
        balance = balance - amount - fee;
        return balance;
    }

    //-----------------------
    // Adds interests to the account and returns the new balance
    //-------------------------
    public double addInterest()//利息
    {
        balance += (balance * RATE);
        return balance;
    }

    //---------------------------------------
    // Returns the current balance of the account
    //------------------------------
    public double getBalance()//这个用户现在的余额
   {
        return balance;
    }





    //----------------------------------
    //  Return  a one -line description of the account as a string
    //------------------------------------
    public String toString()//返回存入的钱，现在账户里的钱。
    {
        NumberFormat fmt = NumberFormat.getCurrencyInstance();
        return "账号: "+acctNumber +"\n\t"+"姓名: "+name +"\n\t"+"此时余额: "+fmt.format(balance);
    }


}
