
import java.util.Scanner;
//-------------------------------------------------------
//编写一个程序，读取0~50（包含两者）范围内任意多个整数，
//并且计算每项输入数据出现的次数，用一个该范围之外的值
//表明输入结束。当输入完成后，输出所有的值及其出现次数。
//--------------------------------------------------------
public class pp81{
    public static void main(String[] args)
    {
        final int LIMIT = 51;
        Scanner scan = new Scanner(System.in);
        int n=0;
        int length = 0;

        int[] Integer = new int[LIMIT];
        int[] Count = new int[LIMIT];


        System.out.println("The input is stopped when the value is greater than 50.");
        System.out.println("Enter number:");
        while(n<=50)
        {
            n = scan.nextInt();
            Count[length] = n;
            length++;
        }

        for(int num=0;num<length;num++)
            if(Count[num]<=50&&Count[num]>=0)
                Integer[Count[num]]++;

        for(int num=0;num<Integer.length;num++)
        {
            System.out.print(num);
            System.out.println(": "+Integer[num]);
        }








    }
}
