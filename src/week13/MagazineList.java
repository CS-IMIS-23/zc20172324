import week10.Sorting;


import java.util.ArrayList;

public class MagazineList {
    private MagazineNode list;

    //----------------------------------------------------------------
    //  Sets up an initially empty list of magazines.
    //----------------------------------------------------------------
    public MagazineList() {
        list = null;
    }

    //----------------------------------------------------------------
    //  Creates a new MagazineNode object and adds it to the end of
    //  the linked list.
    //----------------------------------------------------------------
    public void add(Magazine mag) {
        MagazineNode node = new MagazineNode(mag);
        MagazineNode current;

        if (list == null)
            list = node;
        else {
            current = list;
            while (current.next != null)
                current = current.next;
            current.next = node;
        }
    }

    //----------------------------------------------------------------
    //  Returns this list of magazines as a string.
    //----------------------------------------------------------------
    public String toString() {
        String result = "";

        MagazineNode current = list;

        while (current != null) {
            result += current.magazine + "\n";
            current = current.next;
        }

        return result;
    }

    //*****************************************************************
    //  An inner class that represents a node in the magazine list.
    //  The public variables are accessed by the MagazineList class.
    //*****************************************************************
    private class MagazineNode {
        public Magazine magazine;
        public MagazineNode next;

        //--------------------------------------------------------------
        //  Sets up the node
        //--------------------------------------------------------------
        public MagazineNode(Magazine mag) {
            magazine = mag;
            next = null;
        }

//        public Comparable getTitle() {
//        }
    }

    public void insert(int index, Magazine newMagazine) {
        MagazineNode node = new MagazineNode(newMagazine);
        MagazineNode current = list;

        if (list == null)
            list = node;
        else {
            current = list;
            if (index == 0) {
                node.next = current;
                list = node;
            } else {
                int x = 0;
                while (x < index-1) {
                    current = current.next;
                    x++;
                }
                node.next = current.next;
                current.next = node;
            }
        }
    }

    public void delete(Magazine delNode) {
        MagazineNode current = list;
        if (String.valueOf(current.magazine).equals(String.valueOf(delNode))) {
            list = current.next;
        } else {
            while (!(String.valueOf(current.next.magazine).equals(String.valueOf(delNode)))) {
                current = current.next;
            }
            current.next = current.next.next;
        }
        if (!current.magazine.equals(delNode) && current.next == null) {
            System.out.println("找到节点，删除成功");
        }
    }

    public void Sort()
    {
        int n =0;
        ArrayList<Comparable> sort = new ArrayList();
        while (list!=null)
        {
            sort.add(n,list.magazine);
            list = list.next;
            n++;
        }
        n = 0;
        Comparable[] list = new Comparable[sort.size()];
        while (n<sort.size())
        {
            list[n] = sort.get(n);
            n++;
        }
        Sorting.insertionSort(list);
        for (Comparable a : list)
            System.out.println(a);
    }


}

