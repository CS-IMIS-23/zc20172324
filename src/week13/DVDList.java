package week13;
import week10.Sorting;
import java.util.ArrayList;

public class DVDList {
    private DVDNode list;

    //----------------------------------------------------------------
    //  Sets up an initially empty list of magazines.
    //----------------------------------------------------------------
    public DVDList() {
        list = null;
    }

    //----------------------------------------------------------------
    //  Creates a new MagazineNode object and adds it to the end of
    //  the linked list.
    //--------------------------------------------------------------
    public void add(DVD dvd) {
        DVDNode node = new DVDNode(dvd);
        DVDNode current;

        if (list == null)
            list = node;
        else {
            current = list;
            while (current.next != null)
                current = current.next;
            current.next = node;
        }
    }

    //----------------------------------------------------------------
    //  Returns this list of magazines as a string.
    //----------------------------------------------------------------
    public String toString() {
        String result = "";

        DVDNode current = list;

        while (current != null) {
            result += current.dvd + "\n";
            current = current.next;
        }

        return result;
    }


    //*****************************************************************
    //  An inner class that represents a node in the magazine list.
    //  The public variables are accessed by the MagazineList class.
    //****************************************************************
    private class DVDNode {
        public DVD dvd;
        public DVDNode next;

        //--------------------------------------------------------------
        //  Sets up the node
        //--------------------------------------------------------------
        public DVDNode(DVD dvd) {
            this.dvd = dvd;
            next = null;
        }


    }
}
