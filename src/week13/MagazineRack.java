public class MagazineRack
{
    //----------------------------------------------------------------
    //  Creates a MagazineList object, adds several magazines to the
    //  list, then prints it.
    //----------------------------------------------------------------
    public static void main(String[] args)
    {
        MagazineList rack = new MagazineList();
        System.out.println("正常");
        rack.add(new Magazine("Time"));
        rack.add(new Magazine("Woodworking Today"));
        rack.add(new Magazine("Communications of the ACM"));
        rack.add(new Magazine("House and Garden"));
        rack.add(new Magazine("GQ"));
        System.out.println(rack);

        System.out.println("删除");
        rack.delete(new Magazine("GQ"));
        rack.delete(new Magazine("Communications of the ACM"));


        System.out.println(rack);
        System.out.println("添加");
        rack.insert(0, new Magazine("dw"));
        rack.insert(1, new Magazine("hh of the ACM"));
        rack.insert(4, new Magazine("gs"));
        System.out.println(rack);

        System.out.println("排序");
        rack.Sort();





    }
}

