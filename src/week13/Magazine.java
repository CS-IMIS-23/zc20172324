public class Magazine implements Comparable{

    private String title;

    public String getTitle() {
        return title;
    }


    public int compareTo(Object magazine) {
        int result;
        result = title.compareTo(((Magazine)magazine).getTitle());
        return result;
    }


    //-----------------------------------------------------------------
    //  Sets up the new magazine with its title.
    //-----------------------------------------------------------------
    public Magazine(String newTitle)
    {
        title = newTitle;
    }

    //-----------------------------------------------------------------
    //  Returns this magazine as a string.
    //-----------------------------------------------------------------
    public String toString()
    {
        return title;
    }


}

