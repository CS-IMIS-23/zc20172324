package week13;

public class DVDRack {
    public static void main(String[] args) {
        DVDList rack = new DVDList();
        rack.add(new DVD("The Godfather","Francis Ford Copppla",1972,24.95,true));
        rack.add(new DVD("District9","Neill Blomkamp",2009,19.95,false));
        rack.add(new DVD("Iron Man","Jon Favreau",2008,15.95,false));
        rack.add(new DVD("All About Eve","Joseph Mankiewicz",1950,17.50,false));
        rack.add(new DVD("The Matrix","Andy&Lana Wachowski",1999,19.95,true));

        System.out.println(rack);
    }
}
