package chap10;

public class BinaryTreeNode<T>
{
    protected T element;
    protected BinaryTreeNode<T> left, right;

    /**
     * Creates a new tree node with the specified data.
     *
     * @param obj the element that will become a part of the new tree node
     */
    public BinaryTreeNode(T obj)
    {
        element = obj;
        left = null;
        right = null;
    }

    /**
     * Creates a new tree node with the specified data.
     *
     * @param obj the element that will become a part of the new tree node
     * @param left the tree that will be the left subtree of this node
     * @param right the tree that will be the right subtree of this node
     */
    public BinaryTreeNode(T obj, LinkedBinaryTree<T> left, LinkedBinaryTree<T> right)
    {
        element = obj;
        if (left == null)
            this.left = null;
        else
            this.left = left.getRootNode();

        if (right == null)
            this.right = null;
        else
            this.right = right.getRootNode();
    }

    /**
     * Returns the number of non-null children of this node.
     *
     * @return the integer number of non-null children of this node
     */
    public int numChildren()
    {
        int children = 0;

        if (left != null)
            children = 1 + left.numChildren();

        if (right != null)
            children = children + 1 + right.numChildren();

        return children;
    }

    /**
     * Return the element at this node.
     *
     * @return the element stored at this node
     */
    public T getElement()
    {
        return element;
    }

    /**
     * Return the right child of this node.
     *
     * @return the right child of this node
     */
    public BinaryTreeNode<T> getRight()
    {
        return right;
    }

    /**
     * Sets the right child of this node.
     *
     * @param node the right child of this node
     */
    public void setRight(BinaryTreeNode<T> node)
    {
        right = node;
    }

    /**
     * Return the left child of this node.
     *
     * @return the left child of the node
     */
    public BinaryTreeNode<T> getLeft()
    {
        return left;
    }

    /**
     * Sets the left child of this node.
     *
     * @param node the left child of this node
     */
    public void setLeft(BinaryTreeNode<T> node)
    {
        left = node;
    }
    public void setElement(BinaryTreeNode<T> node) { element = (T) node; }
    //为BinaryTreeNode类创建布尔方法，用于判定该结点是一片叶子还是一个内部结点。
    //若是内部结点就返回false；是叶子就返回true；
    public boolean judge()
    {
        BinaryTreeNode<T>  judgenode = null;
        if(judgenode.numChildren()!=0)
            return false;
        else
            return true;
    }

    public void inorder (ArrayIterator<T> iter)
    {
        if (left != null)
            left.inorder (iter);
        iter.add (element);
        if (right != null)
            right.inorder (iter);
    }
    public void postorder(ArrayIterator<T> iter) {
        if (left != null)
            left.inorder (iter);
        if (right != null)
            right.inorder (iter);
        iter.add (element);
    }
    public void preorder (ArrayIterator<T> iter)
    {
        iter.add (element);
        if (left != null)
            left.preorder (iter);
        if (right != null)
            right.preorder (iter);
    }
    public BinaryTreeNode<T> find (T target)
    {
        BinaryTreeNode<T> result = null;
        if (element.equals(target))
            result = this;
        else
        {
            if (left != null)
                result = left.find(target);
            if (result == null && right != null)
                result = right.find(target);
        }
        return result;
    }
    public BinaryTreeNode<T> remove(T target)
    {
        BinaryTreeNode<T> result = this;

        if(Integer.parseInt(String.valueOf(target))==Integer.parseInt(String.valueOf(element)))
        {
            if (left == null && right == null)
                result = null;
            else if (left != null && right == null)
                result = (BinaryTreeNode)left;
            else if (left == null && right != null)
                result = (BinaryTreeNode)right;
            else
            {
                result = getSuccessor();
                result.left = left;
                result.right = right;
            }
        }
        else
        if ((Integer.parseInt(String.valueOf(target)))<(Integer.parseInt(String.valueOf(element))) )
            if (left != null)
                left = ((BinaryTreeNode)left).remove(target);
            else
            if (right != null)
                right = ((BinaryTreeNode)right).remove(target);

        return result;
    }
    protected BinaryTreeNode<T> getSuccessor()
    {
        BinaryTreeNode<T> successor = (BinaryTreeNode)right;
        while (successor.getLeft() != null)
            successor = (BinaryTreeNode) successor.getLeft();
        ((BinaryTreeNode)right).remove (successor.getElement());
        return successor;
    }
}
