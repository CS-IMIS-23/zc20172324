package chap10;

public class ExpressionTree extends LinkedBinaryTree<ExpressionTreeOp> {

    /**
     * 无参构造函数
     *
     */
    public ExpressionTree() {
        super();
    }

    /**
     * expressionTree的构造函数
     *
     * @param eobj
     * @param left
     * @param right
     */
    public ExpressionTree(ExpressionTreeOp eobj, ExpressionTree left, ExpressionTree right) {
        root = new BinaryTreeNode<ExpressionTreeOp>(eobj, left, right);
    }

    /**
     * 表达式树的计算
     *
     * @return
     */
    public int evaluateTree() {
        return evaluateNode(root);
    }

    /**
     * 结点的递归计算
     *
     * @param root
     * @return
     */
    private int evaluateNode(BinaryTreeNode<ExpressionTreeOp> root) {
        int result, op1, op2;

        ExpressionTreeOp temp;
        if (root == null) {
            result = 0;
        } else {
            temp = (ExpressionTreeOp) root.getElement();
            if (temp.isOperator()) {
                op1 = evaluateNode(root.getLeft());
                op2 = evaluateNode(root.getRight());
                result = computeTerm(temp.getOperator(), op1, op2);
            }else{
                result = temp.getValue();
            }

        }

        return result;
    }

    /**
     * 计算规则的定义实现
     *
     * @param operator
     * @param op1
     * @param op2
     * @return
     */
    private int computeTerm(char operator, int op1, int op2) {
        int result = 0;
        if (operator == '+') {
            result = op1 + op2;

        } else if (operator == '-') {
            result = op1 - op2;

        } else if (operator == '*') {
            result = op1 * op2;

        } else if (operator == '/') {
            result = op1 / op2;

        }

        return result;
    }

    public String printTree() {
        UnorderedListADT<BinaryTreeNode<ExpressionTreeOp>> nodes = new ArrayUnorderedList<BinaryTreeNode<ExpressionTreeOp>>();
        UnorderedListADT<Integer> levelList = new ArrayUnorderedList<Integer>();

        BinaryTreeNode<ExpressionTreeOp> current;
        String result = "";
        int printDepth = this.getHeight();
        int possibleNodes = (int) Math.pow(2, printDepth + 1);
        int countNodes = 0;

        nodes.addToRear(root);
        Integer currentLevel = 0;
        Integer previousLevel = -1;
        levelList.addToRear(currentLevel);

        while (countNodes < possibleNodes) {
            countNodes = countNodes + 1;
            current = nodes.removeFirst();
            currentLevel = levelList.removeFirst();
            if (currentLevel > previousLevel) {
                result = result + "\n\n";
                previousLevel = currentLevel;
                for (int j = 0; j < ((Math.pow(2, (printDepth - currentLevel))) - 1); j++)
                    result = result + " ";
            } else {
                for (int i = 0; i < ((Math.pow(2,
                        (printDepth - currentLevel + 1)) - 1)); i++) {
                    result = result + " ";
                }
            }
            if (current != null) {
                result = result + (current.getElement()).toString();
                nodes.addToRear(current.getLeft());
                levelList.addToRear(currentLevel + 1);
                nodes.addToRear(current.getRight());
                levelList.addToRear(currentLevel + 1);
            } else {
                nodes.addToRear(null);
                levelList.addToRear(currentLevel + 1);
                nodes.addToRear(null);
                levelList.addToRear(currentLevel + 1);
                result = result + " ";
            }

        }

        return result;
    }

}