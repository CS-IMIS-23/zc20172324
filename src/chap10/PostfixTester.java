package chap10;

import java.util.Scanner;


public class PostfixTester {

    public static void main(String[] args) {
        String expression, again;
        int result;

        Scanner in = new Scanner(System.in);

        PostfixEvaluator evaluator = new PostfixEvaluator();

        expression = "5 3 - 4 * 9 +";
        result = evaluator.evaluate(expression);
        System.out.println();
        System.out.println("That expression equals " + result);

        System.out.println("The Expression Tree for that expression is: ");
        System.out.println(evaluator.getTree());

    }

}
