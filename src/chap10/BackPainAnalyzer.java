package chap10;
import java.io.*;

/**
 * BackPainAnaylyzer demonstrates the use of a binary decision tree to
 * diagnose back pain.
 */
public class BackPainAnalyzer
{
    /**
     *  Asks questions of the user to diagnose a medical problem.
     */
    public static void main (String[] args) throws FileNotFoundException
    {
        System.out.println ("So, you're having back pain.");

        DecisionTree expert = new DecisionTree("C:\\Users\\96553\\IdeaProjects\\zc\\src\\chap10\\input.txt");
        expert.evaluate();
        System.out.println("the number of leaf is :"+ expert.LeafCount());
        System.out.println("the height of the tree is :"+ expert.Height());
        System.out.println();
        expert.printtree1();

        System.out.println();
        expert.printtree2();
    }
}


