package HttpSocket;


import java.io.*;
        import java.net.ServerSocket;
        import java.net.Socket;

/**
 * Created by besti on 2018/6/9.
 */

import com.sun.corba.se.impl.orbutil.ObjectUtility;

import java.io.*;
import java.net.Socket;

/**
 * Created by besti on 2018/6/9.
 */
public class SocketClient {
    public static void main(String[] args) throws IOException {
        //1.建立客户端Socket连接，指定服务器位置和端口
//        Socket socket = new Socket("localhost",8080);
       Socket socket = new Socket("172.16.43.243",8800);
       // Socket socket = new Socket("172.16.43.180",8800);


        //2.得到socket读写流
        OutputStream outputStream = socket.getOutputStream();
        //       PrintWriter printWriter = new PrintWriter(outputStream);
        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(outputStream);
        //输入流
        InputStream inputStream = socket.getInputStream();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream,"UTF-8"));
        //3.利用流按照一定的操作，对socket进行读写操作
        String info1 = " User name：Giao，password：wosinidegiaogiao";
        String info2 = "2 52 14 3 8 4 21 2 9";
        String info = new String(info1.getBytes("utf-8"));
        System.out.println();
        String info3 = new String(info2.getBytes("utf-8"));

        //     printWriter.write(info);
        //     printWriter.flush();
       // outputStreamWriter.write(info);
        outputStreamWriter.write(info3);
        outputStreamWriter.flush();
        socket.shutdownOutput();
        //接收服务器的响应
        String reply = null;
        while (!((reply = bufferedReader.readLine()) ==null)){
            System.out.println();
            System.out.println("排序后的数字顺序是：" + reply);

        }
        //4.关闭资源
        bufferedReader.close();
        inputStream.close();
        outputStreamWriter.close();
        //printWriter.close();
        outputStream.close();
        socket.close();
    }
}