package chap6;

import java.io.PrintWriter;

public class Product implements Comparable<Product> {

    private String Name;
    private int Price;


    public Product(int price,String name) {
        Name = name;
        Price = price;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public int getPrice() {
        return Price;
    }

    public void setPrice(int price) {
        Price = price;
    }

    public String toString() {
        return  Name + " "  + Price+"\n";
    }

    @Override
    public int compareTo(Product o) {
        if (Name.compareTo(o.getName()) > 0)
            return 1;
        else {
            if (Name.compareTo(o.getName()) < 0)
                return -1;
            else {
                if (Price>o.getPrice())
                    return 1;
                else
                    return -1;
            }
        }
    }
}