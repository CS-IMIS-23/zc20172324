package chap6;

import chap3.EmptyCollectionException;

public class OrderedArray<T> extends ArrayList<T>
        implements OrderedListADT<T>
{
    /**
     * Creates an empty list using the default capacity.
     */
    public OrderedArray()
    {
        super();
    }

    /**
     * Creates an empty list using the specified capacity.
     *
     * @param initialCapacity the initial size of the list
     */
    public OrderedArray(int initialCapacity)
    {
        super(initialCapacity);
    }

    /**
     * Adds the specified Comparable element to this list, keeping
     * the elements in sorted order.
     *
     * @param element the element to be added to the list
     */
    public void add(T element)
    {
        if (!(element instanceof Comparable))
            throw new NonComparableElementException("OrderedList");

        Comparable<T> comparableElement = (Comparable<T>)element;

        if (size() == list.length)
            expandCapacity();

        int scan = 0;

        // find the insertion location
        while (scan < rear && comparableElement.compareTo(list[scan]) > 0)
            scan++;

        // shift existing elements up one
        for (int shift=rear; shift > scan; shift--)
            list[shift] = list[shift-1];

        // insert element
        list[scan] = element;
        rear++;
        modCount++;
    }

    @Override
    public T removeFirst() throws EmptyCollectionException
    {
        if (isEmpty()){
            throw new EmptyCollectionException("ArrayList");
        }

        T result ;
        result= list[0];
        for (int i = 0; i < rear - 1;i++){
            list[i] = list[i + 1];
        }

        list[rear - 1] = null;
        rear--;
        modCount++;

        return result;
    }

    @Override
    public T removeLast() throws EmptyCollectionException
    {
        if (isEmpty()){
            throw new EmptyCollectionException("ArrayList");
        }

        T result ;
        result= list[list.length-1];
        list[rear] = null;
        rear--;
        modCount++;
        return result;
    }
}

