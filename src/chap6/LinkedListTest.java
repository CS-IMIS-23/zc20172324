package chap6;

public class LinkedListTest {
    public static void main(String[] args) {
        //无序列表的测试
        UnorderedList ul = new UnorderedList();
        System.out.println("无序列表的测试");
        System.out.println("在队列前端插入");
        ul.addToFront("工作");
        System.out.println("输出无序列表：" + ul.toString());

        System.out.println("在队列后端插入");
        ul.addToRear("做不完的");
        System.out.println("输出无序列表：" + ul.toString());

        System.out.println("在\"工作\"后面插入\"是永远\"");
        ul.addAfter("是永远","工作");
        System.out.println("输出无序列表：" + ul.toString());


        //有序列表的测试
        System.out.println("有序列表的测试");
        OrderedList ol = new OrderedList();
        ol.add(3);
        ol.add(9);
        ol.add(1);
        ol.add(6);
        ol.add(0);
        System.out.println("输出有序列表"+ol.toString());


    }
}
