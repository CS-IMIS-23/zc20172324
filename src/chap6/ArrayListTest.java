package chap6;

public class ArrayListTest {
    public static void main(String[] args) {
        //无序列表
        UnorderedArray ua = new UnorderedArray();
        System.out.println("无序列表的测试");
        System.out.println("在队列头添加");
        ua.addToFront("今天");
        System.out.println("输出无序列表：" + ua.toString());

        System.out.println("在队列尾添加");
        ua.addToRear("最后一天");
        System.out.println("输出无序列表：" + ua.toString());

        System.out.println("在\"今天\"后面添加\"是国庆\"");
        ua.addAfter("是国庆", "今天");
        System.out.println("输出无序列表：" + ua.toString());

        System.out.println("在队尾添加");
        ua.addToRear("呜呜呜");
        System.out.println("输出无序列表：" + ua.toString());

        System.out.println("删除队头");
        ua.removeFirst();
        System.out.println("输出无序列表：" + ua.toString());

        System.out.println("删除队尾");
        ua.removeLast();
        System.out.println("输出无序列表：" + ua.toString());

        System.out.println("删除\"是国庆\"");
        ua.remove("是国庆");
        System.out.println("输出无序列表：" + ua.toString());

        //有序列表
        System.out.println("有序列表的测试");
        OrderedArray oa = new OrderedArray();
        oa.add("a");
        oa.add("v");
        oa.add("o");
        oa.add("b");
        oa.add("d");
        System.out.println("输出有序列表：" +oa.toString());
        System.out.print("列表是否为空");
        System.out.println(oa.isEmpty()); ;
        System.out.print("列表元素个数");
        System.out.println(oa.size());;
        System.out.print("第一个元素");
        System.out.println(oa.first());;
        System.out.print("最后一个元素");
        System.out.println(oa.last());;
        System.out.println("输出有序列表：" +oa.toString());



    }
}
