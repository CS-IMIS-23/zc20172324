package chap6;

import java.io.IOException;

public class POSTester2 {
    public static void main(String[] args) throws IOException {
    ProgramOfStudy2 pos = new ProgramOfStudy2();
    System.out.println("判断是否为空："+pos.isEmpty());
    System.out.println("列表长度："+pos.size());
    pos.addCourse(new Course("CS",101,"Introduction to Programming","A-"));
    pos.addCourse(new Course("ARCH",305,"Building Analysis","A"));
    pos.addCourse(new Course("GER",210,"Intermediate German"));
    pos.addCourse(new Course("CS",320,"Computer Architecture"));
    pos.addCourse(new Course("THE",201,"The Theatre Experience"));
    System.out.println("打印课表");
    System.out.println(pos.toString());

    Course contains = new Course("CS",320,"Computer Architecture");
    System.out.println("判断是否有CS 320,\"Computer Architecture\"");
    pos.contains(contains);
    System.out.println("第一个列表元素："+pos.first());
    System.out.println("最后一个列表元素："+pos.last());

    pos.remove(contains);
    System.out.println("去掉元素CS 320,\"Computer Architecture\"后的列表："+"\n "+pos.toString());
    pos.removeFirst();
    pos.removeLast();
    System.out.println("去掉头和尾之后的列表："+"\n"+pos.toString());



}
}
