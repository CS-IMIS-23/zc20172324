package chap6;

import java.io.Serializable;


public class Course  implements Serializable,Comparable<Course>{
    private String prefix;
    private int number;
    private String title;
    private String grade;
    //用一些特殊变量构造这个课程函数
    public Course(String prefix,int number,String title,String grade)
    {
        this.prefix = prefix;
        this.number = number;
        this.title = title;
        if(grade == null)
            this.grade ="";
        else
            this.grade=grade;
    }
    //用不包含成绩grade的特殊变量构造课程函数
    public Course(String prefix,int number,String title)
    {
        this(prefix,number,title,"");
    }
    //返回课程名称的前缀
    public String getPrefix()
    {
        return prefix;
    }
    //返回课程名称的数字
    public int getNumber()
    {
        return number;
    }
    //返回课程名称
    public String getTitle()
    {
        return title;
    }
    //返回课程的成绩
    public String getGrade()
    {
        return grade;
    }
    //将此门课程的成绩传送给指定的地方
    public void setGrade(String grade)
    {
        this.grade = grade;
    }
    //如果这门课被选择就返回true，如果这个成绩被承认，被接受。
    public boolean taken()
    {
        return !grade.equals("");
    }
    //决定这门课程是否与指定的另一门课程相等，根据指定课程的前缀和数字
    public boolean equals(Object other)
    {
        boolean result = false;
        if(other instanceof Course)
        {
            Course otherCourse = (Course)other;
            if(prefix.equals(otherCourse.getPrefix())&&number==otherCourse.getNumber())
                result = true;
        }
        return result;
    }
    //创造和打印代表这个课程的字符串
    public String toString(){
        String result = prefix+" "+ number+"："+title;
        if(!grade.equals(""))
            result += "["+grade+"]";
        return result;
    }

    @Override
    public int compareTo(Course o) {
        if (prefix.compareTo(o.prefix) > 0)
        {
            return 1;
        }
        else
        {
            if (prefix.compareTo(o.prefix) < 0)
            {
                return -1;
            }
            else
            {
                if (number > o.number)
                {
                    return 1;
                }
                else
                {
                    if (number < o.number) {
                        return -1;
                    }
                    else {
                        return 0;
                    }
                }
            }
        }
    }
}


