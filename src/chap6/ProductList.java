package chap6;

import chap3.EmptyCollectionException;
import chap4.LinearNode;

public class ProductList<T> {
    protected int count;
    protected LinearNode<Product> head, tail;

    public ProductList() {
        count = 0;
        head = tail = null;
    }

    public void add(Product product)
    {
        LinearNode<Product> pro = new LinearNode<Product>(product);
        if (isEmpty())
        {
            head=tail=pro;
        }
        else {
            tail.setNext(pro);
            tail = pro;
        }
        count++;
    }

    public void addToFront(Product element)
    {
        LinearNode node = new LinearNode((T) element);
        if (head == null) {
            head = node;
            tail = node;
        }
        else {
            node.setNext(head);
            head = node;
        }
        count++;
    }

    public void addToRear(Product element){
        LinearNode node = new LinearNode(element);
        if(isEmpty())
            head = node;
        else
            tail.setNext(node);
        tail = node;
        count++;
    }

    public Product removeFirst() throws EmptyCollectionException
    {
        if (isEmpty()){
            throw new EmptyCollectionException("LinkedList");
        }

        LinearNode node = head;
        Product result = (Product) node.getElement();
        head = head.getNext();

        count--;
        return result;
    }

    public void insert(Product element1, int A) {
        LinearNode<Product> node1 = new LinearNode<Product>(element1);
        LinearNode<Product> current = head;

        if (A == 0) {
            node1.setNext(current);
            head = node1;
        } else {
            for (int i = 1; i < A; i++)
                current = current.getNext();
            node1.setNext(current.getNext());
            current.setNext(node1);
        }
        count++;

    }

    public Product removeLast() throws EmptyCollectionException
    {
        if (isEmpty()){
            throw new EmptyCollectionException("LinkedList");
        }

        LinearNode node = head;
        Product result = (Product) node.getElement();
        while (node.getNext() != tail){
            node = node.getNext();
        }
        tail = node;

        count--;
        return result;
    }

    public Product remove(T targetElement) throws EmptyCollectionException,
            ElementNotFoundException
    {
        if (isEmpty())
            throw new EmptyCollectionException("LinkedList");

        boolean found = false;
        LinearNode previous = null;
        LinearNode current = head;

        while (current != null && !found)
            if (targetElement.equals(current.getElement()))
                found = true;
            else
            {
                previous = current;
                current = current.getNext();
            }

        if (!found)
            throw new ElementNotFoundException("LinkedList");

        if (size() == 1)  // only one element in the list
            head = tail = null;
        else if (current.equals(head))  // target is at the head
            head = current.getNext();
        else if (current.equals(tail))  // target is at the tail
        {
            tail = previous;
            tail.setNext(null);
        }
        else  // target is in the middle
            previous.setNext(current.getNext());

        count--;
        return (Product) current.getElement();
    }

    public Product first() throws EmptyCollectionException
    {
        if (isEmpty())
            throw new EmptyCollectionException("LinkedList");

        return head.getElement();
    }

    public Product last() throws EmptyCollectionException
    {
        if (isEmpty()){
            throw new EmptyCollectionException("LinkedList");
        }

        LinearNode temp = head;
        while(temp.getNext()!= null) {
            temp = temp.getNext();
        }
        tail = temp;
        return tail.getElement();
    }

    public boolean contains(T targetElement) throws
            EmptyCollectionException {
        if (isEmpty())
            throw new EmptyCollectionException("LinkedList");

        boolean search = false;
        LinearNode current = new LinearNode();
        while (current.getNext() != null) {
            current = current.getNext();
        }
        if (current == targetElement)
            return search;
        else
            return !search;

    }

    public Product find(String target) {
        LinearNode<Product> temp = head;
        while (temp != null && !temp.getElement().getName().equals(target)) {
            temp = temp.getNext();
        }
        return temp.getElement();
    }

    public void SelectSort() {
        LinearNode<Product> min;
        Product pro;
        LinearNode<Product> temp = head;
        while (temp != null) {
            min = temp;
            LinearNode<Product> current = min.getNext();
            while (current != null) {
                if (current.getElement().compareTo(min.getElement()) < 0) {
                    min = current;
                }
                current = current.getNext();
            }
            pro = min.getElement();
            min.setElement(temp.getElement());
            temp.setElement(pro);
            temp = temp.getNext();
        }
    }

    public boolean isEmpty()
    {
        if(size()==0)
            return true;
        else
            return false;
    }

    public int size()
    {
        return count;
    }

    public String toString()
    {
        String result = "";
        LinearNode node = head;
        for(int a=0;a<count;a++)
        {
            result += node.getElement()+" ";
            node = node.getNext();
        }

        return result;
    }


}


