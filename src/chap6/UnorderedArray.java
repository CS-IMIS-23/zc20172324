package chap6;


import chap3.EmptyCollectionException;

import java.util.Arrays;

public class UnorderedArray<T> extends ArrayList<T> implements UnorderedListADT<T> {
    public UnorderedArray() {
        super();
    }

    public UnorderedArray(int initialCapacity) {
        super(initialCapacity);
    }

    @Override
    public void addToFront(T element) {
        // 对数组进行扩容
        if (size() == list.length) {
            expandCapacity();
        }
        int scan = 0;
        // 将现有元素向上移动1个
        for (int shift = rear; shift > scan; shift--) {
            list[shift] = list[shift - 1];
        }
        // 插入元素
        list[scan] = element;
        rear++;
        modCount++;
    }

    @Override
    public void addToRear(T element) {
        if (size() == list.length) {
            expandCapacity();
        }
        list[rear] = element;
        rear++;
        modCount++;
    }

    @Override
    public void addAfter(T element, T target) {
        if (size() == list.length) {
            expandCapacity();
        }
        int scan = 0;

        // 查找插入位置
        while (scan < rear && !target.equals(list[scan])) {
            scan++;
        }
        if (scan == rear) {
            throw new ElementNotFoundException("UnorderedList");
        }
        scan++;

        // 将现有元素向上移动1个
        for (int shift = rear; shift > scan; shift--) {
            list[shift] = list[shift - 1];
        }
        // 插入元素
        list[scan] = element;
        rear++;
        modCount++;
    }

    protected void expandCapacity() {
        list = Arrays.copyOf(list, list.length * 2);
    }

    @Override
    public T removeFirst() throws EmptyCollectionException
    {
        if (isEmpty()){
            throw new EmptyCollectionException("ArrayList");
        }

        T result ;
        result= list[0];
        for (int i = 0; i < rear - 1;i++){
            list[i] = list[i + 1];
        }

        list[rear - 1] = null;
        rear--;
        modCount++;

        return result;
    }

    @Override
    public T removeLast() throws EmptyCollectionException
    {
        if (isEmpty()){
            throw new EmptyCollectionException("ArrayList");
        }

        T result ;
        result= list[list.length-1];
        list[rear] = null;
        rear--;
        modCount++;
        return result;
    }
}



