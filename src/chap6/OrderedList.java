package chap6;

import chap4.LinearNode;

public class OrderedList<T>  extends LinkedList<T> implements OrderedListADT<T>
 {
     public OrderedList()
     {
         super();
     }


     public void add(T element)
     {
         LinearNode<T> temp = new LinearNode(element);
         LinearNode<T> previous = null;
         LinearNode<T> current = head;
         while(current != null && Integer.parseInt(element+"") > Integer.parseInt(current.getElement()+"")){
             previous = current;
             current = current.getNext();
         }

         if(previous == null){
             head = tail =  temp;
         }
         else{
             previous.setNext(temp);
         }
         temp.setNext(current);
         if(temp.getNext() == null)
             tail = temp;


         count++;
         modCount++;

     }
 }

