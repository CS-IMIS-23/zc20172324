package chap6;

import chap3.EmptyCollectionException;
import chap4.LinearNode;

import java.io.*;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class ProgramOfStudy2 implements Iterable<Course> {
    protected LinearNode<Course> head, tail;
    protected int count,modCount;

    public ProgramOfStudy2()
    {
        count = 0;
        head = tail = null;
        modCount = 0;
    }

    public void addCourse(Course o)
    {
        LinearNode<Course> current = head;
        LinearNode<Course> previous = null;
        LinearNode<Course> node = new LinearNode<>(o);


        while (( current != null) && (o.compareTo(current.getElement()) > 0))
        {
            previous = current;
            current = current.getNext();
        }
        if (previous == null )
        {
            head = node;
        }
        else {
            previous.setNext(node);
        }
        node.setNext(current);
        count++;
    }
    public Course first() throws EmptyCollectionException {
        return head.getElement();
    }


    public Course last() {
        Course result = tail.getElement();
        return result;
    }

    public boolean contains(Course targetElement) throws
            EmptyCollectionException {
        if (isEmpty())
            throw new EmptyCollectionException("LinkedList");

        boolean found = false;
        LinearNode<Course> previous = null;
        LinearNode<Course> current = head;

        while (current != null && !found)
            if (targetElement.equals(current.getElement()))
                found = true;
            else {
                previous = current;
                current = current.getNext();
            }

        if (!found)
            return false;
        else
            return true;
    }
    public Course removeFirst() {
        if(isEmpty())
        {
            throw new EmptyCollectionException("LinkedList");
        }
        LinearNode<Course> current = head;
        head = current.getNext();

        count--;

        return current.getElement();
    }

    public Course removeLast() {
        LinearNode<Course> previous = null;
        LinearNode<Course> current = head;
        if(isEmpty())
        {
            throw new EmptyCollectionException("LinkedList");
        }
        while (current != null)
        {
            previous = current;
            current = current.getNext();
        }

        tail = previous;
        tail.setNext(null);

        count--;

        return current.getElement();
    }

    public Course remove(Course targetElement) throws EmptyCollectionException,
            ElementNotFoundException {
        if (isEmpty())
            throw new EmptyCollectionException("LinkedList");

        boolean found = false;
        LinearNode<Course> previous = null;
        LinearNode<Course> current = head;

        while (current != null && !found)
            if (targetElement.equals(current.getElement()))
                found = true;
            else {
                previous = current;
                current = current.getNext();
            }

        if (!found)
            throw new ElementNotFoundException("LinkedList");

        if (size() == 1)
            head = tail = null;
        else if (current.equals(head))
            head = current.getNext();
        else if (current.equals(tail)) {
            tail = previous;
            tail.setNext(null);
        } else
            previous.setNext(current.getNext());

        count--;
        modCount++;

        return current.getElement();
    }
    public int size() {
        return count;
    }

    public boolean isEmpty() {
        if (count == 0)
            return true;
        else
            return false;
    }

//    public Course find(String prefix,int number)
//    {
//        for(Course course:list)
//            if(prefix.equals(course.getPrefix())&& number==course.getNumber())
//                return course;
//
//        return null;
//    }
//
//    public void addCourseAfter(Course target,Course newCourse)
//    {
//        if(target == null || newCourse == null)
//            return;
//
//        int targetIndex= list.indexOf(target);
//        if(targetIndex != -1)
//            list.add(targetIndex + 1,newCourse);
//    }
//
//    public void replace(Course target,Course newCourse)
//    {
//        if(target == null || newCourse == null)
//            return;
//
//        int targetIndex= list.indexOf(target);
//        if(targetIndex != -1)
//            list.set(targetIndex ,newCourse);
//    }

    public String toString()
    {
        int n = count;
        String result = "";
        LinearNode<Course> current = head;
        while(n > 0){
            result += current.getElement()+"\n";
            current = current.getNext();
            n--;
        }
        return result;
    }



    public void save(String filename) throws IOException
    {
        FileOutputStream fos = new FileOutputStream(filename);
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.writeObject(this);
        oos.flush();
        oos.close();
    }



    public static ProgramOfStudy load(String fileName) throws IOException,ClassNotFoundException
    {
        FileInputStream fis = new FileInputStream(fileName);
        ObjectInputStream ois = new ObjectInputStream(fis);
        ProgramOfStudy pos = (ProgramOfStudy)ois.readObject();
        ois.close();

        return pos;
    }

    @Override
    public Iterator<Course> iterator() {
        return null;
    }
}
