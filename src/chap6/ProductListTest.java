package chap6;

public class ProductListTest {
    public static void main(String[] args) {
        ProductList pl = new ProductList();
        pl.add(new Product(7600,"computer"));
        pl.add(new Product(6900,"iphone Xr"));
        pl.add(new Product(180,"mac chili"));
        pl.add(new Product(67,"ears rings"));

        System.out.println("商品名单:");
        System.out.println(pl.toString());

        System.out.println("在商品名单前添加沙发");
        pl.addToFront(new Product(13000,"sofa"));
        System.out.println("在商品名单后添加书包");
        pl.addToRear(new Product(200,"mschf"));
        System.out.println("在第三个位置添加白菜");
        pl.insert(new Product(3,"vegetable"),2);

        System.out.println("商品名单:");
        System.out.println(pl.toString());

        System.out.println("根据商品首字母排序");
        pl.SelectSort();
        System.out.println("排序后结果为");
        System.out.println(pl.toString());

        System.out.println("删除第一个");
        pl.removeFirst();
        System.out.println("删除最后一个");
        pl.removeLast();

        System.out.println("商品名单:");
        System.out.println(pl.toString());

        System.out.println("查找vegetable");
       System.out.println("查找结果： "+"\n"+ pl.contains("vegetable"));








    }
}
