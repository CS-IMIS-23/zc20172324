package chap6;

import chap4.LinearNode;

public class UnorderedList<T> extends LinkedList<T>  implements UnorderedListADT<T>
{

    public UnorderedList()
    {
        super();
    }

    @Override
    public void addToFront(T element) {
        LinearNode<T> lt = new LinearNode<T>(element);
        lt.setNext(head);
        head = lt;
        count++;
        modCount++;

    }

    @Override
    public void addToRear(T element){
            LinearNode<T> lt = new LinearNode<T>(element);
            lt.setNext(null);
            tail.setNext(lt);
            count++;
            modCount++;

        }



    @Override
    public void addAfter(T element, T target) {
        LinearNode<T> node = new LinearNode<T>(element);
        LinearNode<T> temp = head;
        if (temp == null) {
            head = tail = node;
        }
        while (temp.getElement() == target) {
            temp = temp.getNext();
        }
        if (temp.getNext() == null) {
            temp.setNext(node);
            tail = node;
        } else {
            node.setNext(temp.getNext());
            temp.setNext(node);
        }
        count++;
        modCount++;
    }
}