package exp3;

import java.util.Scanner;

public class OddorEven
{ //------------------------------------------------------------------
//输入一个整数，输出之中偶数、奇数、零的个数
//------------------------------------------------------------------
    public static void main(String[] args)
    {
        Integer num;
        int i;
        String I;

        Scanner scan = new Scanner(System.in);
        System.out.print("输入一个整数:");
        num = scan.nextInt();

        int odd = 0;//奇数个数
        int even = 0;//偶数个数
        int zero = 0;//零的个数

        String Num = String.valueOf(num);//把num转换为字符串
        int N = Num.length();

        for (int count = 0; count < N; count ++)
        {
            I = Num.substring(count, count + 1);
            i = Integer.parseInt(I);

            if (i == 0){
                zero ++;}
                else
                if (i != 0 && i % 2 == 1) {
                    odd ++;}
                 if (i != 0 && i % 2 == 0){
                    even ++;}
        }

        System.out.println("奇数个数:" + odd);
        System.out.println("偶数个数:" + even);
        System.out.println("零的个数:" + zero);
    }
//God bless
}