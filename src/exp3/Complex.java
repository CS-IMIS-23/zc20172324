package exp3;


public class Complex {
    // 定义属性并生成getter,setter
    private double RealPart;//实部
    private double ImagePart;//虚部

    // 定义构造函数
    public Complex(double R, double I) {
        this.RealPart = R;
        this.ImagePart = I;
    }

    //Override Object
    public boolean equals(Complex a) {
        boolean result = false;
        if (RealPart == a.RealPart && ImagePart == a.ImagePart) {
            result = true;
        }
        return result;
    }

    public double getRealPart() {
        return RealPart;
    }

    public double getImagPart() {
        return ImagePart;
    }

    // 定义公有方法:加减乘除
    Complex ComplexAdd(Complex a) {
        return new Complex(this.RealPart + a.RealPart, this.ImagePart + a.ImagePart);
    }

    Complex ComplexSub(Complex a) {
        return new Complex(this.RealPart - a.RealPart, this.ImagePart - a.ImagePart);
    }

    Complex ComplexMulti(Complex a) {
        return new Complex(this.RealPart * a.RealPart - this.ImagePart * a.ImagePart,
                this.ImagePart * a.RealPart + this.RealPart * a.ImagePart);
    }

    Complex ComplexDiv(Complex a) {
        return new Complex((this.RealPart * a.RealPart + this.ImagePart * a.ImagePart) / (a.RealPart * a.RealPart + a.ImagePart * a.ImagePart),
                (this.ImagePart * a.RealPart - this.RealPart * a.ImagePart) / (a.RealPart * a.RealPart + a.ImagePart * a.ImagePart));
    }

    @Override
    public String toString() {
        String result;
        if (ImagePart > 0) {
            result = RealPart + "+" + ImagePart + "i";
        }
        else {
            if (ImagePart < 0) {
                result = RealPart + "+" + ImagePart + "i";
            }
            else {
                result = RealPart + " ";
            }
        }
        return result;
    }
}