public class randomColor {
    public int RED;
    public int GREEN;
    public int BLUE;

    public randomColor(int R, int G, int B) {
        RED = R;
        GREEN = G;
        BLUE = B;
    }

    public void setRED(int R) {
        RED = R;
    }

    public void setGREEN(int G) {
        GREEN = G;
    }

    public void setBLUE(int B) {
        BLUE = B;
    }

    public long getRED() {
        return RED;
    }

    public long getGREEN() {
        return GREEN;
    }

    public long getBLUE() {
        return BLUE;
    }

    public String toString()
    {
        return String.valueOf(RED+"\t"+GREEN+"\t"+BLUE);
    }

}

