import java.util.Scanner;
public class pp2_12
{
//------------------------------------------------------------------------------//Enter an integer as the side length of the square
//------------------------------------------------------------------------------
public static void main(String[] args)
{
float a;
Scanner scan = new Scanner (System.in);
System.out.print("Enter the number of a: ");
a = scan.nextFloat();
System.out.println("Square area: " + (a * a));
System.out.println("Square perimeter: " + (4 * a));
}
}

