package cn.edu.besti.cs172324;

public class Searching {
    public static <T extends Comparable<? super T>>
        //线性法查找
    boolean linearSearch(T[] data, int min, int max, T target)
    {
        int index = min;
        boolean found = false;

        while (!found && index <= max)
        {
            found = data[index].equals(target);
            index++;
        }

        return found;
    }
}
