package cn.edu.besti.cs172324;

import java.lang.reflect.Array;
import java.util.Arrays;

public class Sorting {
    public  <T extends Comparable<T>> T selectionSort(T[] data)
    {
        int min;


        for (int index = 0; index < data.length-1; index++)
        {
            min = index;
            for (int scan = index+1; scan < data.length; scan++)
                if (data[scan].compareTo(data[min])<0)
                    min = scan;

            swap(data, min, index);
        }
        return (T) Arrays.toString(data);
    }

    //互换
    static <T extends Comparable<T>>
    void swap(T[] data, int index1, int index2)
    {
        T temp = data[index1];
        data[index1] = data[index2];
        data[index2] = temp;
    }
}
