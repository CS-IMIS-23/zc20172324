import java.io.*;
import java.util.Scanner;

public class sorttext {
    public static void main(String[] args) throws IOException {
        try {
            BufferedReader buf = new BufferedReader(new InputStreamReader(System.in));
            BufferedWriter buf2 = new BufferedWriter(new FileWriter("sort.txt"));
            System.out.print("请输入你想要排序的数字:");
            String str = buf.readLine();
            String[] array = str.split(" ");//spilt的作用是将输入的字符串按照你规定的符号分隔开来
            //并返回到一个数组里 长知识了哇！
            System.out.print("你输入的数字是：");
            for (int i = 0; i < array.length; i++) {
                array[i] = array[i].trim();
                System.out.print(array[i] + " ");

            }
            buf2.write(str);
            buf.close();
            buf2.close();

            InputStreamReader reader = new InputStreamReader(new FileInputStream("sort.txt"));
            BufferedReader sort = new BufferedReader(reader);
            String line = "";
            line = sort.readLine();
            while (line != null) {
                line = sort.readLine(); // 一次读入一行数据
            }
            selectionSort(array);
            System.out.println();
            System.out.print("排序后的结果是：");
            for (String num : array)
                System.out.print(num + " ");
            String arraystr = toString(array);

            BufferedWriter buf4 = new BufferedWriter(new FileWriter("sort.txt", true));
            buf4.write(arraystr);
            buf4.close();
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    public static void selectionSort(Comparable[] list) {
        int min;
        Comparable temp;


        for (int index = 0; index < list.length - 1; index++) {
            min = index;
            for (int scan = index + 1; scan < list.length; scan++)
                if (list[scan].compareTo(list[min]) < 0)
                    min = scan;

            // Swap the values
            temp = list[min];
            list[min] = list[index];
            list[index] = temp;
        }
    }

    public static String toString(String[] array) {
        String temp = "           排序后:";
        for (int x = 0; x < array.length; x++) {
            temp += array[x] + " ";

        }
        return temp;
    }
}

