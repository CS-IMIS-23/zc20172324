public class Sheep extends Animal {
    public Sheep(String name, int id) {
        super(name, id);
    }

    @Override
    public void eat() {
        System.out.println(name+"正在吃");

    }

    @Override
    public void sleep() {
        System.out.println(name+"正在睡");

    }

    @Override
    public void introduction() {
        System.out.println("大家好！我是"+name+"我的id是"+id);


    }


}

