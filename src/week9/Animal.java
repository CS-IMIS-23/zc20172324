public abstract class Animal {
    protected String name;
    protected int id;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Animal(String name, int id) {

        this.name = name;

        this.id = id;
    }

    public abstract void eat();
    public abstract void sleep();
    public abstract void introduction();




//    public String toString(){
//        String result;
//        result = "名字: "+name+"\t"+"id号: "+id;
//        result += eat()+sleep()+introduction();
//        return result;
//    }
}


