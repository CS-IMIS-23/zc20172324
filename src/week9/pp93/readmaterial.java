public class readmaterial
{
    String name;
    int pages;
    String keywords;

    public readmaterial(String name,int pages,String keywords)
    {
        this.name = name;
        this.pages = pages;
        this.keywords = keywords;
    }
    public void getName(String name)
    {
        this.name = name;
    }
    public void getPages(int pages)
    {
        this.pages = pages;
    }
    public void getKeywords(String keywords)
    {
        this.keywords = keywords;
    }

    public String getName()
    {
        return name;
    }

    public int getPages()
    {
        return pages;
    }

    public String getKeywords()
    {
        return keywords;
    }
    public String toString()
    {
        return "name: "+name+"\t"+"pages: "+pages+"\t"+"keywords: "+keywords;
    }
}

