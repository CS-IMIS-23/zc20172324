package 实验1;

import chap3.EmptyCollectionException;
import chap4.Int;
import chap4.LinearNode;
import chap5.QueueADT;
import 实验1.LinkedListADT;

import java.io.*;
import java.util.Scanner;
import java.util.StringTokenizer;


public class exp1_3LinkedList<T> implements LinkedListADT<T> {
    private int nZengCheng;
    private LinearNode<T> tail, head;

    public exp1_3LinkedList() {
        tail = null;
        head = null;
        nZengCheng = 0;
    }

    @Override
    public void add(T element) {
        LinearNode<T> node = new LinearNode(element);
        LinearNode<T> temp;

        if (head == null) {
            head = node;
        } else {
            temp = head;
            while (temp.getNext() != null) {
                temp = temp.getNext();
            }
            temp.setNext(node);
        }
        nZengCheng++;
    }

    public T dequeue() throws EmptyCollectionException {
        if (isEmpty())
            throw new EmptyCollectionException("Queue is empty.");

        T result = head.getElement();
        head = head.getNext();
        tail = null;
        nZengCheng--;
        return result;
    }

    public T first() {
        return head.getElement();
    }


    public boolean isEmpty() {
        return nZengCheng == 0;
    }


    @Override
    public int size() {
        return nZengCheng;
    }

    public String toString() {
        String result = "";

        for (LinearNode current = this.head; current != null; current = current.getNext()) {
            result = result + current.getElement() + " ";
        }
        return result;
    }

    public void insert(T element, int n) throws Exception {
        if (n > nZengCheng)
            throw new Exception("您想插入的位置不存在，您可选择的区域为0--" + nZengCheng);

        LinearNode<T> node1 = new LinearNode<T>(element);
        LinearNode<T> current = head;
        if (n == 0) {
            node1.setNext(current);
            head = node1;
        } else {
            for (int i = 0; i < n - 1; i++)
                current = current.getNext();
            node1.setNext(current.getNext());
            current.setNext(node1);
        }


        nZengCheng++;
    }

    public void delete(int n) {
        LinearNode pre, temp = null;
        if (isEmpty())
            throw new EmptyCollectionException("Queue is empty.");

        if (n == 0) {
            temp = head;
            head = head.getNext();
            temp.setElement(null);
        } else {
            pre = head;
            temp = head.getNext();
            for (int i = 0; i < n; i++) {
                temp = pre;
                pre = pre.getNext();
            }
            temp.setNext(pre.getNext());
        }

        nZengCheng--;
    }

    public void  selectsort() {
        String result = "";
        String[] str = this.toString().split(" ");
        int[] a = new int[nZengCheng];

//        for(int i = 0; i < str.length - 1; i++) {// 做第i趟排序
//            int k = i;
//            for(int j = k + 1; j < str.length; j++){// 选最小的记录
//                if(Integer.parseInt( str[j]) <Integer.parseInt(str[k])){
//                    k = j; //记下目前找到的最小值所在的位置
//                }
//            }
//            //在内层循环结束，也就是找到本轮循环的最小的数以后，再进行交换
//            if(i != k){  //交换a[i]和a[k]
//                int temp = Integer.parseInt(str[i]);
//                str[i] = str[k];
//                str[k] = String.valueOf(temp);
//            }
//        }
//        String st =" ";
//        for(String num:str){
//            System.out.print(num+" ");
//        }
//
//
//    }
        for (int i = 0; i < a.length - 1; i++) {
            int y = Integer.parseInt(str[i]);
            a[i] = y;
        }

        for (int i = 0; i < a.length - 1; i++) {
            for (int j = i + 1; j < a.length; j++) {
                if (a[i] < a[j]) {
                    int min = a[i];
                    a[i] = a[j];
                    a[j] = min;
                }
            }
            String st = "";
            for (int s = 0; s < a.length; s++) {
                st += a[s] + " ";
            }
            int n = i + 1;
            System.out.print("第" + n + "次后排序的链表为：" + st);
            System.out.println("元素总数为：" + a.length);
        }

    }



    public static void main(String[] args) throws Exception {
        File file = new File("C:\\Users\\96553\\Desktop\\zc", "file");
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }


        exp1_3LinkedList a = new exp1_3LinkedList();
        System.out.println("请输入一系列整数，并用空格隔开：");
        String b;
        Scanner scan = new Scanner(System.in);
        b = scan.nextLine();
        //StringTokenizer(String str)。默认以” \t\n\r\f”（前有一个空格，引号不是）为分割符。
        StringTokenizer st = new StringTokenizer(b);
        // String[] stArray =st.split
        while (st.hasMoreTokens()) {
            a.add(st.nextToken());
        }


        System.out.println("打印所有数字:");
        System.out.println(a.toString());
        System.out.println("元素的总和:" + a.size());

        String num = "1 2 ";

        Writer writer = new FileWriter(file);
        writer.write(num);
        writer.flush();

        Reader reader = new FileReader(file);
        System.out.println();
        System.out.print("此时文件中的内容是：");
        while (reader.ready()) {
            System.out.print((char) reader.read());
        }

        writer.close();
        System.out.println();

        //从文件中读入数字1，  插入到链表第 5 位，并打印所有数字，和元素的总数
        System.out.println("从文件中读入数字1，插入到链表第 5 位");
        StringTokenizer str = new StringTokenizer(num, " ");
        int num1 = Integer.parseInt(str.nextToken());
        a.insert(num1, 4);
        System.out.println("打印所有数字:");
        System.out.println(a.toString());
        System.out.println("元素的总和:" + a.size());

        //从文件中读入数字2， 插入到链表第 0 位，并打印所有数字，和元素的总数。
        System.out.println("从文件中读入数字2， 插入到链表第 0 位");
        int num2 = Integer.parseInt(str.nextToken());
        a.insert(num2, 0);
        System.out.println("打印所有数字:");
        System.out.println(a.toString());
        System.out.println("元素的总和:" + a.size());

        //从链表中删除刚才的数字1.  并打印所有数字和元素的总数。
        System.out.println("从链表中删除刚才的数字1.  并打印所有数字和元素的总数");
        a.delete(5);
        System.out.println("打印所有数字:");
        System.out.println(a.toString());
        System.out.println("元素的总和:" + a.size());

        a.selectsort();
    //    System.out.println(a.selectsort());
    }
}