package 实验1;


import chap3.EmptyCollectionException;
import chap4.LinearNode;
import java.util.Scanner;
import java.util.StringTokenizer;

public class exp1_1LinkedList<T> implements LinkedListADT<T> {
    private int nZengCheng;
    private LinearNode<T> tail, head;

    public exp1_1LinkedList(){
        tail = null;
        head = null;
        nZengCheng = 0;
    }

    //    public void enqueue(T element) {
//        LinearNode<T> node = new LinearNode<T>(element);
//
//        if (isEmpty())
//            head = node;
//        else
//            tail.setNext(node);
//        tail = node;
//        nZengCheng++;
//    }
    @Override
    public void add(T element){
       LinearNode<T> node = new LinearNode(element);
       LinearNode<T> temp;

       if (head == null) {
          head = node;
        }
        else {
        temp = head;
        while (temp.getNext() != null) {
            temp = temp.getNext();
        }
        temp.setNext(node);
        }
         nZengCheng ++;
    }

    public T dequeue() throws EmptyCollectionException {
        if (isEmpty())
            throw new EmptyCollectionException("Queue is empty.");

        T result = head.getElement();
        head = head.getNext();
        tail = null;
        nZengCheng--;
        return result;
    }

    public T first() {
        return head.getElement();
    }


    public boolean isEmpty() {
        if(nZengCheng!=0)
            return false;
        else
            return true;
    }


    public String Print() {
        return null;
    }

    @Override
    public int size() {
        return nZengCheng;
    }

    public String toString(){
        String result = "";

        for(LinearNode current = this.head; current != null; current = current.getNext()) {
            result = result + current.getElement() + " ";
        }
        return result;
    }

    public static void main(String[] args) {
        exp1_1LinkedList a = new exp1_1LinkedList();
        System.out.println("请输入一系列整数，并用空格隔开：");
        String b;
        Scanner scan = new Scanner(System.in);
        b = scan.nextLine();
        //StringTokenizer(String str)。默认以” \t\n\r\f”（前有一个空格，引号不是）为分割符。
        StringTokenizer st = new StringTokenizer(b);
        while (st.hasMoreTokens()) {
            a.add(st.nextToken());
        }
        System.out.print("链表为: ");
        System.out.println(a);
        System.out.println("链表元素为: "+a.size());


    }
}
