package 实验1;


import chap3.EmptyCollectionException;
import chap4.LinearNode;
import chap5.QueueADT;

import java.io.*;
import java.util.Scanner;
import java.util.StringTokenizer;


public class exp1_2LinkedList<T> implements LinkedListADT<T> {
    private int nZengCheng;
    private LinearNode<T> tail, head;

    public exp1_2LinkedList(){
        tail = null;
        head = null;
        nZengCheng = 0;
    }

    @Override
    public void add(T element){
        LinearNode<T> node = new LinearNode(element);
        LinearNode<T> temp;

        if (head == null) {
            head = node;
        }
        else {
            temp = head;
            while (temp.getNext() != null) {
                temp = temp.getNext();
            }
            temp.setNext(node);
        }
        nZengCheng ++;
    }

    public T dequeue() throws EmptyCollectionException {
        if (isEmpty())
            throw new EmptyCollectionException("Queue is empty.");

        T result = head.getElement();
        head = head.getNext();
        tail = null;
        nZengCheng--;
        return result;
    }

    public T first() {
        return head.getElement();
    }


    public boolean isEmpty() {
        return nZengCheng == 0;
    }


    @Override
    public int size() {
        return nZengCheng;
    }

    public String toString(){
        String result = "";

        for(LinearNode current = this.head; current != null; current = current.getNext()) {
            result = result + current.getElement() + " ";
        }
        return result;
    }

        public void insert(T element, int n )throws Exception
    {
        if(n>nZengCheng)
          throw new Exception("您想插入的位置不存在，您可选择的区域为0--"+nZengCheng);

        LinearNode<T> node1 = new LinearNode<T>(element);
        LinearNode<T> current = head;
        if(n==0){
            node1.setNext(current);
            head = node1;
        }else {
            for (int i = 0; i < n - 1; i++)
                current = current.getNext();
            node1.setNext(current.getNext());
            current.setNext(node1);
        }


        nZengCheng++;
    }

    public void delete(int n){
        LinearNode pre, temp = null;
        if (isEmpty())
            throw new EmptyCollectionException("Queue is empty.");

        if( n == 0) {
            temp = head;
            head = head.getNext();
            temp.setElement(null);
        }else {
            pre = head;
            temp = head.getNext();
            for (int i = 0; i < n; i++) {
                temp = pre;
                pre = pre.getNext();
            }
            temp.setNext(pre.getNext());
        }

        nZengCheng --;
    }


    public static void main(String[] args) throws Exception {
        File file = new File("C:\\Users\\96553\\Desktop\\zc", "file");
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }



        exp1_2LinkedList a = new exp1_2LinkedList();
        System.out.println("请输入一系列整数，并用空格隔开：");
        String b;
        Scanner scan = new Scanner(System.in);
        b = scan.nextLine();
        //StringTokenizer(String str)。默认以” \t\n\r\f”（前有一个空格，引号不是）为分割符。
        StringTokenizer st = new StringTokenizer(b);
        // String[] stArray =st.split
        while (st.hasMoreTokens()) {
            a.add(st.nextToken());
        }


        System.out.println("打印所有数字:");
        System.out.println(a.toString());
        System.out.println("元素的总和:" + a.size());

        String num = "1 2 ";

        Writer writer = new FileWriter(file);
        writer.write(num);
        writer.flush();

        Reader reader = new FileReader(file);
        System.out.println();
        System.out.print("此时文件中的内容是：");
        while (reader.ready()) {
            System.out.print((char) reader.read());
        }

        writer.close();
        System.out.println();

        //从文件中读入数字1，  插入到链表第 5 位，并打印所有数字，和元素的总数
        System.out.println("从文件中读入数字1，插入到链表第 5 位");
        StringTokenizer str = new StringTokenizer(num, " ");
        int num1 = Integer.parseInt(str.nextToken());
        a.insert(num1, 4);
        System.out.println("打印所有数字:");
        System.out.println(a.toString());
        System.out.println("元素的总和:" + a.size());

        //从文件中读入数字2， 插入到链表第 0 位，并打印所有数字，和元素的总数。
        System.out.println("从文件中读入数字2， 插入到链表第 0 位");
        int num2 = Integer.parseInt(str.nextToken());
        a.insert(num2, 0);
        System.out.println("打印所有数字:");
        System.out.println(a.toString());
        System.out.println("元素的总和:" + a.size());

        //从链表中删除刚才的数字1.  并打印所有数字和元素的总数。
        System.out.println("从链表中删除刚才的数字1.  并打印所有数字和元素的总数");
        a.delete(5);
        System.out.println("打印所有数字:");
        System.out.println(a.toString());
        System.out.println("元素的总和:" + a.size());

//        exp1_2LinkedQueue a= new exp1_2LinkedQueue();
//        a.enqueue(2);
//        a.enqueue(8);
//        a.enqueue(0);
//        a.enqueue(1);
//        a.enqueue(9);
//        System.out.println("第一次输出所有元素："+a.toString());
//
//        System.out.println("删除元素");
//        a.delete(2);
//        System.out.println("第二次输出所有元素："+a.toString());
//        System.out.println("插入元素");
//        a.insert(2,3);
//        System.out.println("第三次输出所有元素："+a.toString());

    }
}