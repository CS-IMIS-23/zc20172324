package 实验1;


import chap3.EmptyCollectionException;
import java.io.*;
import java.util.Arrays;
import java.util.Scanner;
import java.util.StringTokenizer;

public class exp1_5ArrayQueue<T> {
    private final int DEFAULT_CAPACITY = 100;

    private int nZengCheng;
    private T[] stack;

    public exp1_5ArrayQueue() {
        nZengCheng = 0;
        stack = (T[]) (new Object[DEFAULT_CAPACITY]);
    }

    public exp1_5ArrayQueue(int initialCapacity) {
        nZengCheng = 0;
        stack = (T[]) (new Object[initialCapacity]);
    }

    public  void push(T element)
    {
        if(size() == stack.length)
            expandCapacity();
        stack[nZengCheng] = element;
        nZengCheng++;
    }

    public void insert(T element, int n){
        T[] stack2 = (T[])(new Object[size() + 1]);
        for(int i =0; i < n;i ++)
            stack2[i] = stack[i];
        stack2[n] = element;
        for(int i = n + 1;i < size() + 1; i ++ ) {
            stack2[i] = stack[i - 1];
        }
        stack = stack2;
        nZengCheng ++;
    }

    private void expandCapacity() {
        stack = Arrays.copyOf(stack, stack.length * 2);
    }


    public void delete(int n){
        T[] stack2 = (T[])(new Object[size() - 1 ]);
        for(int i = 0;i < n; i++)
            stack2[i] = stack[i];
        for(int i = n;i < size() - 1; i++)
            stack2[i] = stack[i+1];
        stack = stack2;
        nZengCheng--;
    }

    public T peek() throws EmptyCollectionException {
        if (isEmpty()) {
            throw new EmptyCollectionException("Stack");
        }
        return stack[nZengCheng - 1];
    }

    public T first() {
        return stack[0];
    }

    public boolean isEmpty() {
        if (nZengCheng == 0) {
            return true;
        } else {
            return false;
        }
    }


    public int size() {
        return nZengCheng;
    }

    public String toString() {
        String result = " ";
        for (int index = 0; index < size(); index++) {
            result = result + stack[index] + " ";
        }
        return result;
    }

    public void bobSort(){
        for(int i=0;i<nZengCheng-1;i++){//排序轮数
            for(int j=0;j<nZengCheng-1-i;j++){//比较次数
                if(Integer.parseInt(stack[j].toString())  >Integer.parseInt( stack[j+1].toString())){
                    T temp = stack[j+1];
                    stack[j+1] = stack[j];
                    stack[j] = temp;
                }
                int n = i+1;
                System.out.print("第" + n + "次排序后的数组为： ");
                for (int a = 0;a < nZengCheng;a++){
                    String str = "";
                    str += stack[a] + " ";
                    System.out.print(str);
                }
                System.out.println();
                System.out.println("元素总数为： " + nZengCheng);
            }
        }
    }



        public static void main(String[] args) throws IOException {
        File file = new File("C:\\Users\\96553\\Desktop\\zc", "file");
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }


        exp1_5ArrayQueue a = new exp1_5ArrayQueue();
        System.out.println("请输入一系列整数，并用空格隔开：");
        String b;
        Scanner scan = new Scanner(System.in);
        b = scan.nextLine();
        //StringTokenizer(String str)。默认以” \t\n\r\f”（前有一个空格，引号不是）为分割符。
        StringTokenizer st = new StringTokenizer(b);
        while (st.hasMoreTokens()) {
            a.push(st.nextToken());
        }
        System.out.println("打印所有数字:");
        System.out.println(a.toString());
        System.out.println("元素的总和:" + a.size());

        String num = "1 2 ";

        Writer writer = new FileWriter(file);
        writer.write(num);
        writer.flush();

        Reader reader = new FileReader(file);
        System.out.println();
        System.out.print("此时文件中的内容是：");
        while (reader.ready()) {
            System.out.print((char) reader.read());
        }

        writer.close();
        System.out.println();

        //从文件中读入数字1，  插入到链表第 5 位，并打印所有数字，和元素的总数
        System.out.println("从文件中读入数字1，插入到链表第 5 位");
        StringTokenizer str = new StringTokenizer(num, " ");
        int num1 = (Integer.parseInt(str.nextToken()));
        a.insert(num1,4 );
        System.out.println("打印所有数字:");
        System.out.println(a.toString());
        System.out.println("元素的总和:" + a.size());

        //从文件中读入数字2， 插入到链表第 0 位，并打印所有数字，和元素的总数。
        System.out.println("从文件中读入数字2， 插入到链表第 0 位");
        int num2 = (Integer.parseInt(str.nextToken()));
        a.insert(num2, 0);
        System.out.println("打印所有数字:");
        System.out.println(a.toString());
        System.out.println("元素的总和:" + a.size());

        //从链表中删除刚才的数字1.  并打印所有数字和元素的总数。
        System.out.println("从链表中删除刚才的数字1.  并打印所有数字和元素的总数");
        a.delete(5);
        System.out.println("打印所有数字:");
        System.out.println(a.toString());
        System.out.println("元素的总和:" + a.size());


        a.bobSort();

    }
}